import sys, os

project_path = os.getcwd()
source_path = os.path.join(project_path, "imageit")
sys.path.append(source_path)
