from pathlib import Path
import xarray as xr
import numpy as np
import pytest
import warnings


from imageit.models.surface_models import (  # pylint: disable=import-error
    Surface,
    SurfaceTimeSeries,
    RGBSurface,
)

from imageit.models.geological_models import (  # pylint: disable=import-error
    GeologicalVoxelModel,
)


class TestSurface:

    tif_file = Path(__file__).parent.joinpath(r"data\krwy_bottom.tif")
    temp_folder = Path(__file__).parent.joinpath(r"data")

    @pytest.fixture
    def test_surface(self):
        x_coors = np.arange(130000, 140000, 100)
        y_coors = np.arange(450000, 440000, -100)
        xx, yy = np.meshgrid(x_coors, y_coors, sparse=True)
        z = np.sin(xx) * (yy / 1e5 - xx / 1e5) * np.sin(yy)
        return Surface(
            None,
            xr.DataArray(
                z, coords={"y": y_coors, "x": x_coors}, dims=("x", "y")
            ).transpose(),
        )

    @pytest.fixture
    def test_surface_project(self):
        x_coors = np.arange(130000, 140000, 100)
        y_coors = np.arange(450000, 440000, -100)
        xx, yy = np.meshgrid(x_coors, y_coors, sparse=True)
        z = yy / 1e5 - xx / 1e5
        return Surface(
            None,
            xr.DataArray(
                z, coords={"y": y_coors, "x": x_coors}, dims=("x", "y")
            ).transpose(),
        )

    @pytest.mark.classmethod_test
    def test_load_Surface_from_raster_file(self):
        surface_object = Surface.from_raster_file(self.tif_file)
        assert type(surface_object.model) == xr.DataArray

    @pytest.mark.classmethod_test
    def test_from_ahn_tiles(self):
        surface_object = Surface.from_downloaded_ahn_tiles(
            Xmin=132000, Xmax=135000, Ymin=442000, Ymax=444000
        )
        assert type(surface_object.model) == xr.DataArray
        assert surface_object.model.shape == (400, 600)
        # Check against AHN3 5 m DTM max value in this area.
        assert np.isclose(surface_object.max_value, 9.82229614)

    @pytest.mark.properties_test
    def test_properties(self, test_surface):
        assert np.isclose(test_surface.min_value, -3.13197987)
        assert np.isclose(test_surface.max_value, 3.13297869)
        assert all(test_surface.xc == np.arange(130000, 140000, 100))
        assert all(test_surface.yc == np.arange(450000, 440000, -100))
        assert test_surface.dx == 100.0
        assert test_surface.dy == -100.0
        assert test_surface.Xmin == 130000
        assert test_surface.Xmax == 139900
        assert test_surface.Ymin == 440100
        assert test_surface.Ymax == 450000
        assert (test_surface.model_as_array() == test_surface.model.values).all()

    @pytest.mark.basemethods_test
    def test_clip_Surface(self, test_surface):
        clip = test_surface.clip_ds(
            test_surface.model, 132000, 135000, 444000, 442000, ret=True
        )
        assert clip.shape == (21, 31)

    @pytest.mark.basemethods_test
    def test_structured_grid_creation(self, test_surface):
        structured_grid = test_surface.create_basic_structured_grid()
        assert structured_grid.GetBounds() == (
            130000.0,
            139900.0,
            440100.0,
            450000.0,
            -3.1319799423217773,
            3.132978677749634,
        )
        assert structured_grid.GetNumberOfCells() == 9801

    @pytest.mark.basemethods_test
    def test_reproject(self, test_surface):
        # NotImplemented
        assert True

    @pytest.mark.basemethods_test
    def test_to_netcdf(self, test_surface):
        file = self.temp_folder.joinpath("nc_test.nc")
        test_surface.to_netcdf(file)

        # Since file size is asserted, a change in the xarray netcdf-writer may affect the outcome.
        # If this is the case: manually check the temporary file and updated the required file size.
        assert file.stat().st_size == 89252
        file.unlink()

    @pytest.mark.methods_test
    def test_to_vtk_pyvista_normal_without_bottom(self, test_surface):
        file = self.temp_folder.joinpath("vtk_test.vtk")
        test_surface.to_vtk(file, project=None, texture=None, bottom=None, binary=True)

        # Since file size is asserted, a change in the pyvista vtk-writer may affect the outcome.
        # If this is the case: manually check the temporary file and updated the required file size.
        assert file.stat().st_size == 631487
        file.unlink()

    @pytest.mark.methods_test
    def test_to_vtk_pyvista_normal_with_bottom(self, test_surface):
        file = self.temp_folder.joinpath("vtk_test.vtk")
        test_surface.to_vtk(file, project=None, texture=None, bottom=-8, binary=True)

        # Since file size is asserted, a change in the pyvista vtk-writer may affect the outcome.
        # If this is the case: manually check the temporary file and updated the required file size.
        assert file.stat().st_size == 1145119
        file.unlink()

    @pytest.mark.methods_test
    def test_to_vtk_pyvista_projectedvalues_with_bottom(
        self, test_surface, test_surface_project
    ):
        file = self.temp_folder.joinpath("vtk_test.vtk")
        test_surface.to_vtk(
            file, project=test_surface_project, texture=None, bottom=-8, binary=True
        )

        # Since file size is asserted, a change in the pyvista vtk-writer may affect the outcome.
        # If this is the case: manually check the temporary file and updated the required file size.
        assert file.stat().st_size == 1145119
        file.unlink()

    @pytest.mark.methods_test
    def test_to_vtk_pyvista_normal_with_impossible_bottom(self, test_surface):
        file = self.temp_folder.joinpath("vtk_test.vtk")

        with pytest.raises(Warning) as excinfo:
            test_surface.to_vtk(file, project=None, texture=None, bottom=0, binary=True)
        assert (
            "Given bottom value lies above or is equal to minimum height of surface"
            in str(excinfo.value)
        )


class TestGeologicalVoxelModel:

    nc_file = Path(__file__).parent.joinpath(r"data\geotop_test.nc")
    temp_folder = Path(__file__).parent.joinpath(r"data")

    @pytest.fixture
    def test_model(self):
        pass

    @pytest.mark.classmethod_test
    def test_load_Geotop_from_netcdf(self):
        voxel_object = GeologicalVoxelModel.from_netcdf(self.nc_file)
        assert type(voxel_object.model) == xr.Dataset
