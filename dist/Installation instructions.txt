Installation instructions for Imageit. This is a custom installation procedure to avoid the troublesome installation of GDAL and Fiona by using Christoph Gohlke's wheels. (https://www.lfd.uci.edu/~gohlke/pythonlibs/)

Preferably you don't have Anaconda installed...

Erik van Onselen
erik.vanonselen@deltares.nl


INSTRUCTIONS FOR FIRST-TIME INSTALLATION:
----------------------------------------

1. Download and install Python 3.9.7, 64 bit from python.org. Also, make sure that Python 3.9.7 is added to the PATH environment (tick the box during installation)

2. Open the command prompt (search for 'cmd' in the task bar)

3. Change directory to where the three batch files (create, install, activate) and the imageit .whl file are located. 
	- e.g. "cd C:\path\to\folder" without the quotation marks.

4. type "create_imageit" (w/o quotation marks) and press enter
	- The basic environment is now created. A folder named 'imageit_env' should have appeared.
	
5. type "install_imageit" (w/o quotation marks) and press enter
	- All packages will now be downloaded and installed.
	

USING THE ENVIRONMENT:
---------------------

Follow steps 2 and 3 of the above installation guide. Then type "activate" to activate the environment.

You can also choose to use it in an IDE of your choice
