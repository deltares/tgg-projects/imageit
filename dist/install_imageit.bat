python -m pip install -U pip

pip install wheel
pip install pipwin

pipwin refresh

pip install numpy==1.20.3
pipwin install xarray
pipwin install shapely
pipwin install gdal
pipwin install fiona
pipwin install pyproj
pipwin install six
pipwin install rtree
pipwin install geopandas
pipwin install rasterio
pipwin install matplotlib
pipwin install numba
pipwin install tqdm
pipwin install pytest
pipwin install netcdf4

pip install xarray
pip install shapely
pip install gdal
pip install fiona
pip install pyproj
pip install six
pip install rtree
pip install geopandas
pip install rasterio
pip install matplotlib
pip install numba
pip install tqdm
pip install pytest
pip install netcdf4

pip install typing-extensions
pip install pyvista==0.32.0
pip install scipy==1.8.0
pip install simplekml==1.3.6
pip install translate==3.6.1
pip install cipy
pip install owslib
pip install opencv-python

pip install imageit-0.1-py3-none-any.whl
