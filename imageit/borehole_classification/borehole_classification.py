# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 12:54:59 2021

@author: karaouli
"""
import pandas as pd
import pickle
import glob
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
# from plot_boreholes import plot_boreholes
from pathlib import Path
from matplotlib.path import Path as PPath
import matplotlib.patches as patches
from rgbcmyk import imod_c, imod_d
import sys
sys.path.insert(0, r'.\geo_vtk\src')
from vtkclass import VtkClass

class Borehole_classification:
    def __init__(self,path=r'data/geotop/basisdata/',classify=0):
        """        
        This package reads the borehole file with (from BRO, in xls format), makes a selection based on the polygon provided,
        and plots all CPTs in VTK format (to be viewed with Paraview)
        All files will be stored to the results\data for the selected file (pickle fomrat)
        and results\vtk\boreholes.vtk for the graphics output. 

        Parameters
        ----------        
        :datapath Provide path to the xls file
        :polygon Fiona shapefile. 
        :Xmin
        :Xmax
        :Ymin
        :Ymax   
        Returns
        -------
        None.        
        """
        self.path=path
        self.loc=[] # x,y of borehole
        self.bot=[] # borehole information
        self.dsc=[] # borehole desciprtion
        
        self.sel_loc=[]
        self.sel_dsc=[]
        # Check if basis data exist. If nto, read the xls files
        loc=Path(self.path+'borehole_locations.pkl')
        bor=Path(self.path+'borehole.pkl')
        dsc=Path(self.path+'descriptions.pkl')
        if loc.is_file() & bor.is_file() & dsc.is_file():
            with open(self.path+'borehole_locations.pkl', 'rb') as f:
                self.loc = pickle.load(f)
               
            with open(self.path+'borehole.pkl', 'rb') as f:
                self.bor = pickle.load(f)

            with open(self.path+'descriptions.pkl', 'rb') as f:
                self.dsc = pickle.load(f)

        else:
            self.read_basisdata()
        
        if classify==1:
            #Check if file exist
            borehole=Path(self.path+'borehole_analysis.pkl')
            if borehole.is_file():
                with open(self.path+'borehole_analysis.pkl', 'rb') as f:
                    self.borehole = pickle.load(f)
            else:
                self.percentages_boreholes()
                
        
            

        
        
        
    def read_basisdata(self):
        '''
        Read xls raw data and store them

        Returns
        -------
        None.

        '''
             
        
        location=glob.glob(self.path+'GTP_BORE*')
        boreholes=glob.glob(self.path+'GTP_LTH*')
        descriptions=glob.glob(self.path+'GTP_LITHO*')

        for i in range(0,len(boreholes)):
            
            if i==0:
                self.loc=pd.read_excel(location[i])
                self.bor=pd.read_excel(boreholes[i])
                self.dsc=pd.read_excel(descriptions[i])

            else:
                self.loc=self.loc.append(pd.read_excel(location[i]))
                self.bor=self.bor.append(pd.read_excel(boreholes[i]))
                self.dsc=self.dsc.append(pd.read_excel(descriptions[i]))
                
        
        # Assign class to color
        
        self.dsc.loc[self.dsc['LITHO_CLASS_CD']=='k','NO']=2
        self.dsc.loc[self.dsc['LITHO_CLASS_CD']=='kz','NO']=3
        self.dsc.loc[self.dsc['LITHO_CLASS_CD']=='zf','NO']=5
        self.dsc.loc[self.dsc['LITHO_CLASS_CD']=='v','NO']=1
        self.dsc.loc[self.dsc['LITHO_CLASS_CD']=='z','NO']=5  # z is not fine sand?
        self.dsc.loc[self.dsc['LITHO_CLASS_CD']=='zg','NO']=7
        self.dsc.loc[self.dsc['LITHO_CLASS_CD']=='zm','NO']=6
        self.dsc.loc[self.dsc['LITHO_CLASS_CD']=='o','NO']=0
        self.dsc.loc[self.dsc['LITHO_CLASS_CD']=='g','NO']=8
        self.dsc.loc[self.dsc['LITHO_CLASS_CD']=='she','NO']=9

         
        with open(self.path+'borehole_locations.pkl', 'wb') as f:
            pickle.dump(self.loc, f)        
            
            
        with open(self.path+'borehole.pkl', 'wb') as f:
            pickle.dump(self.bor, f)        

        with open(self.path+'descriptions.pkl', 'wb') as f:
            pickle.dump(self.dsc, f)                
        
        
    def percentages_boreholes(self):
        '''
        Assign perentages to geology

        Returns
        -------
        None.

        '''
        
        # now merge main classes that are irrelevant (less than 1%)
        to_be_merged=list(self.bor['LITHOLOGY_CD'].unique())
        percentages_pes_main_class=pd.DataFrame(data=None,columns=('LITHOLOGY_CD','Percentage'))
        for i in range(0,len(to_be_merged)):
            percentages_pes_main_class=percentages_pes_main_class.append(
                pd.DataFrame(data=[[to_be_merged[i] ,
                      100*len(self.bor[self.bor['LITHOLOGY_CD']==to_be_merged[i]])/len(self.bor)]],
                columns=('LITHOLOGY_CD','Percentage') )       
                )
        to_be_merged=percentages_pes_main_class[percentages_pes_main_class['Percentage']<1]
            
        
        # Major classes, Silt, Sand, Gravel, Clay, Humus (make sure we have everyting needed, autmate in futute
        # For now, just check that we have s1,s2,s.... whatever)
        clay_admix_cd=list(self.bor['CLAY_ADMIX_CD'].unique())
        silt_admix_cd=list(self.bor['SILT_ADMIX_CD'].unique())
        sand_admix_cd=list(self.bor['SAND_ADMIX_CD'].unique())
        gravel_admix_cd=list(self.bor['GRAVEL_ADMIX_CD'].unique())
        humus_admix_cd=list(self.bor['HUMUS_ADMIX_CD'].unique())
        
        
        # Assigne percentages and store to new dataframe
        self.boreholes={}
        for i in tqdm(range(0,len(self.loc))):
            
            tmp1=self.loc.iloc[i]
            tmp2=self.bor[ (self.bor['GTP_AREA_NM']==tmp1['GTP_AREA_NM'] ) & (self.bor['NITG_NR']==tmp1['NITG_NR'])    ]
            # tmp2=bor[(bor['NITG_NR']==tmp1['NITG_NR'])    ]
        
            # get main lithology classes
            tmp3=tmp2[['TOP_DEPTH','BOTTOM_DEPTH','LITHOLOGY_CD','CLAY_ADMIX_CD',
                      'SILT_ADMIX_CD','SAND_ADMIX_CD','GRAVEL_ADMIX_CD','HUMUS_ADMIX_CD']]
            
            tmp3.loc[tmp3['LITHOLOGY_CD'].isin(list(to_be_merged['LITHOLOGY_CD'])),'LITHOLOGY_CD']='Other'
            
            
            # convert to percentages
            tmp3=tmp3.replace(np.nan,0)
            # add percentages coloums
            tmp3['CLAY']=0
            tmp3['SILT']=0
            tmp3['SAND']=0
            tmp3['GRAVEL']=0
            tmp3['HUMUS']=0
            tmp3['VEIN']=0
            tmp3['LOSS']=0
            
            tmp3['OTHER']=0
            
            tmp3['REST']=0
        
            # check what will happen, when LITHOLOGY_CD is the same as ADMIX
            tmp3.loc[tmp3['LITHOLOGY_CD']=='K','CLAY_ADMIX_CD']=0
            tmp3.loc[tmp3['LITHOLOGY_CD']=='Z','SAND_ADMIX_CD']=0
        
            
            # manual for easier change
            tmp3.loc[tmp3['SILT_ADMIX_CD']=='S1','SILT']=0.1
            tmp3.loc[tmp3['SILT_ADMIX_CD']=='S2','SILT']=0.2
            tmp3.loc[tmp3['SILT_ADMIX_CD']=='S3','SILT']=0.3
            tmp3.loc[tmp3['SILT_ADMIX_CD']=='S4','SILT']=0.4
            tmp3.loc[tmp3['SILT_ADMIX_CD']=='SX','SILT']=0.05
        
            tmp3.loc[tmp3['CLAY_ADMIX_CD']=='K1','CLAY']=0.1
            tmp3.loc[tmp3['CLAY_ADMIX_CD']=='K2','CLAY']=0.2
            tmp3.loc[tmp3['CLAY_ADMIX_CD']=='K3','CLAY']=0.3
            tmp3.loc[tmp3['CLAY_ADMIX_CD']=='K4','CLAY']=0.4
            tmp3.loc[tmp3['CLAY_ADMIX_CD']=='KX','CLAY']=0.05
            
            tmp3.loc[tmp3['GRAVEL_ADMIX_CD']=='G1','GRAVEL']=0.1
            tmp3.loc[tmp3['GRAVEL_ADMIX_CD']=='G2','GRAVEL']=0.2
            tmp3.loc[tmp3['GRAVEL_ADMIX_CD']=='G3','GRAVEL']=0.3
            tmp3.loc[tmp3['GRAVEL_ADMIX_CD']=='G4','GRAVEL']=0.4
            tmp3.loc[tmp3['GRAVEL_ADMIX_CD']=='GX','GRAVEL']=0.05    
        
            tmp3.loc[tmp3['SAND_ADMIX_CD']=='Z1','SAND']=0.1
            tmp3.loc[tmp3['SAND_ADMIX_CD']=='Z2','SAND']=0.2
            tmp3.loc[tmp3['SAND_ADMIX_CD']=='Z3','SAND']=0.3
            tmp3.loc[tmp3['SAND_ADMIX_CD']=='Z4','SAND']=0.4
            tmp3.loc[tmp3['SAND_ADMIX_CD']=='ZX','SAND']=0.05    
        
            tmp3.loc[tmp3['HUMUS_ADMIX_CD']=='H1','HUMUS']=0.1
            tmp3.loc[tmp3['HUMUS_ADMIX_CD']=='H2','HUMUS']=0.2
            tmp3.loc[tmp3['HUMUS_ADMIX_CD']=='H3','HUMUS']=0.3
            tmp3.loc[tmp3['HUMUS_ADMIX_CD']=='H4','HUMUS']=0.4
            tmp3.loc[tmp3['HUMUS_ADMIX_CD']=='HX','HUMUS']=0.05   
        
            # Main class
            tmp3['REST']=tmp3['SILT'] + tmp3['SAND'] + tmp3['CLAY'] + tmp3['GRAVEL'] +tmp3['HUMUS']
            tmp3.loc[tmp3['LITHOLOGY_CD']=='K','CLAY']=1-tmp3.loc[tmp3['LITHOLOGY_CD']=='K','REST']
            tmp3.loc[tmp3['LITHOLOGY_CD']=='Z','SAND']=1-tmp3.loc[tmp3['LITHOLOGY_CD']=='Z','REST']
            tmp3.loc[tmp3['LITHOLOGY_CD']=='V','VEIN']=1-tmp3.loc[tmp3['LITHOLOGY_CD']=='V','REST']
            tmp3.loc[tmp3['LITHOLOGY_CD']=='L','LOSS']=1-tmp3.loc[tmp3['LITHOLOGY_CD']=='L','REST']
        
            tmp3.loc[tmp3['LITHOLOGY_CD']=='Other','OTHER']=1-tmp3.loc[tmp3['LITHOLOGY_CD']=='Other','REST']
            
        
            
            
            
            #make dataframe to store data
            tmp3={"Location":tmp1,'Description':tmp2,'Percentages':tmp3}
            self.boreholes[i]=tmp3        
        
        
        # save file
        with open(self.path+'borehole_analysis.pkl', 'wb') as f:
            pickle.dump(self.boreholes, f) 
            
            
            
    def plot_boreholes_2d(self,list_plot):
        fig, ax = plt.subplots()
        k=0
        for i in list_plot:   
            self.plot_boreholes_2d(self.boreholes[i],fig,ax,shift=k*1.5)
            k=k+1
        # plt.xlim((-1,30))
        # plt.ylim((-14000,5000))
        plt.ylabel('NAP cm')            
            
            
            

    def plot_boreholes_one_2d(self,pb,fig,ax,shift=0):
        cc=imod_c()
        map_to_imod=[2,1,5,0,8,3,7,4]

        elev=pb['Location']['HEIGHT_NAP']
        no_patches=len(pb['Percentages'])
        
        # fig, ax = plt.subplots()
        for i in range(0,no_patches):
            tmp=pb['Percentages'][['CLAY','VEIN','SAND','LOSS','OTHER','SILT','GRAVEL','HUMUS']]
            cur_start=0
            for k in range(0,tmp.shape[1]):
                if tmp.iloc[i,k]            >0:
                    # tt=tmp.iloc[i,:k]
                    cur_end=np.sum(tmp.iloc[i].values[:k+1])
                
                    verts = [
                    (cur_start+shift, elev-pb['Percentages']['BOTTOM_DEPTH'].iloc[i]),  # left, bottom
                    (cur_start+shift, elev-pb['Percentages']['TOP_DEPTH'].iloc[i]),  # left, top
                    (cur_end+shift, elev-pb['Percentages']['TOP_DEPTH'].iloc[i]),  # right, top
                    (cur_end+shift, elev-pb['Percentages']['BOTTOM_DEPTH'].iloc[i]),  # right, bottom
                    (0., 0.),  # ignored
                                 ]
                    
                    cur_start=cur_end
         
        
                    codes = [
                        PPath.MOVETO,
                        PPath.LINETO,
                        PPath.LINETO,
                        PPath.LINETO,
                        PPath.CLOSEPOLY,
                    ]    
                    
                    
                    path = PPath(verts, codes)    
                
                    
                    patch = patches.PathPatch(path, facecolor=cc[map_to_imod[k],:], lw=2)
                    ax.add_patch(patch)
    
        ax.text(shift+0.375,elev-200,pb['Location']['NITG_NR'],rotation=90)
        # plt.show()
        # ax.xlim((-1,2))
        # ax.ylim((-14000,1000))
        return patch
        
    
    
    def select_boreholes(self,Xmin=0,Ymin=0,Xmax=0,Ymax=0):
        
        
        # data=np.c_[[Xmin,Xmax],[Ymin,Ymax]]
                           
            
                
        p=PPath(np.array([[Xmin,Ymin],
                            [Xmax,Ymin],
                            [Xmax,Ymax],
                            [Xmin,Ymax],
                            [Xmin,Ymin] ]))  
        flags =p.contains_points(np.c_[
            self.loc['X_RD_CRD'],self.loc['Y_RD_CRD']
            
            ])
        
       
        
        
        self.sel_loc=self.loc.iloc[flags]
        self.sel_dsc=self.dsc.loc[self.dsc['NITG_NR'].isin(self.sel_loc['NITG_NR'])]
        
        
        with open('results\data\sel_loc.pkl', 'wb') as f:
            pickle.dump(self.sel_loc, f)    
            
            with open('results\data\sel_descriptions.pkl', 'wb') as f:
                pickle.dump(self.sel_dsc, f)    
        
        
        
        

    def plot_boreholes(self,one=0,radius=20):
        intvv=VtkClass()
        if one==0:
            
            for i in range(0,len(self.sel_loc)):
                data=self.sel_dsc.loc[self.sel_dsc['NITG_NR']==self.sel_loc['NITG_NR'].iloc[i]]
                data=data[['TOP_DEPTH','BOTTOM_DEPTH','NO']].values
                data[:,0]=1e-3*data[:,0]
                data[:,1]=1e-3*data[:,1]
                
                tmp_loc=np.c_[self.sel_loc['X_RD_CRD'].iloc[i],self.sel_loc['Y_RD_CRD'].iloc[i],1e-3*self.sel_loc['HEIGHT_NAP'].iloc[i]]
                tmp_loc=np.tile(tmp_loc, (data.shape[0],1))
                data=np.c_[data[:,:2],tmp_loc,data[:,2]]
                
                if i==0:
                    out=data
                else:
                    out=np.r_[out,data]
            intvv.make_borehole_as_cube_multi('results\\vtk\\boreholes.vtk',out,radius,label=['Geology Class'])
            
        else:
            
            for i in range(0,len(self.sel_loc)):
                data=self.sel_dsc.loc[self.sel_dsc['NITG_NR']==self.sel_loc['NITG_NR'].iloc[i]]
                data=data[['BOTTOM_DEPTH','NO']].values
                data[:,0]=data[:,0]/1000
                intvv.make_borehole_as_cube(filename='results\\vtk\\'+self.sel_loc['NITG_NR'].iloc[i]+'.vtk',
                                            data=data,
                                            center=[self.sel_loc['X_RD_CRD'].iloc[i],self.sel_loc['Y_RD_CRD'].iloc[i]], 
                                            radius=5,
                                            elev=self.sel_loc['HEIGHT_NAP'].iloc[i]/1000)

        
    
            