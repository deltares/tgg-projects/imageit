# -*- coding: utf-8 -*-
"""
Created on Fri Dec 20 12:18:40 2019

@author: karaouli
"""

import pygimli as pg
from pygimli.mplviewer import drawModel1D
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sys
from scipy.signal import savgol_filter
# from scipy import interpolate
# from scipy.interpolate import Rbf
# from scipy.interpolate import interp1d

class Ves_inversion():
    def __init__(self,data,error=1e6):
        
        # self.data=data
        ab2=data['Raw Data']['L/2'].values
        rhoa=data['Raw Data']['R'].values

        # new_=np.power(10,non_uniform_savgol(np.log10(ab2),np.log10(rhoa),7,3))
        # error=100*np.abs(rhoa-new_)/rhoa  
        unique_ab2=np.unique(ab2)
        
        # plt.loglog(ab2,rhoa,'x')  
        # plt.loglog(ab2,new_)
        
        # automatic fiter
        new_rhoa=np.zeros(unique_ab2.shape)
        for i in range(0,len(unique_ab2)):
            ix=np.where(ab2==unique_ab2[i])[0]
            new_rhoa[i]=np.max(rhoa[ix])
        plt.loglog(unique_ab2,new_rhoa,'-o')
        z_new=np.power(10,savgol_filter(np.log10(new_rhoa),7,5))
        error=100*np.abs(np.log10(new_rhoa)-np.log10(z_new))/np.log10(new_rhoa)
        ix=np.where(error<1)[0]
        plt.loglog(unique_ab2[ix],new_rhoa[ix],'x')
        
        self.ab2=unique_ab2[ix]
        self.rhoa=new_rhoa[ix]
        self.mn2=0.5*np.ones(self.ab2.shape)
        self.error=10
        
        

        

        no_models_found=len(data['Inversion Model'])
        self.rhoa_dinoloket={}
        # Check if there are actual some data
        if isinstance(data['Inversion Model'][0],pd.DataFrame):
            for k in range(0,no_models_found):
                # There is a cahnce that the 1st layer in Dinoloket has 0 thickenss
                if(data['Inversion Model'][k]['onder'][0]==0):
                    data['Inversion Model'][k]['onder'].iloc[0]=0.5
                    data['Inversion Model'][k]['boven'].iloc[1]=0.5
                for ii in range(0,len(data['Inversion Model'][k]['onder'])-1):
                    if data['Inversion Model'][k]['onder'].iloc[ii]==data['Inversion Model'][k]['onder'].iloc[ii+1]:
                        data['Inversion Model'][k]['onder'].iloc[ii+1]=data['Inversion Model'][k]['onder'].iloc[ii+1]+0.5
                        data['Inversion Model'][k]['boven'].iloc[ii+2]=data['Inversion Model'][k]['boven'].iloc[ii+2]+0.5
                        
                        
                        
                        
                self.synthk=data['Inversion Model'][k]['onder'].values- data['Inversion Model'][k]['boven'].values
                self.synthk=list(self.synthk[:-1])
                self.synres =list(data['Inversion Model'][k]['Rs'].values)
                nlay = len(self.synres)
                f = pg.DC1dModelling(nlay, self.ab2, self.mn2)
                
                self.rhoa_dinoloket[k] = np.asarray(f(self.synthk+self.synres))
    
        # self.nlay=len(self.synres)
        self.block_id=0
        self.smooth_id=0
        
        self.results={'Response from Dinoloket':self.rhoa_dinoloket,

                      'Response from 8 layers': None,
                      'Response from 7 layers': None,
                      'Response from 6 layers': None,
                      'Response from 5 layers': None,
                      'Response from 4 layers': None,
                      'Response from 3 layers': None,
                      'Response from 2 layers': None,
                      
                      'Model 8 layers': None,
                      'Model 7 layers': None,
                      'Model 6 layers': None,
                      'Model 5 layers': None,
                      'Model 4 layers': None,
                      'Model 3 layers': None,
                      'Model 2 layers': None,
                                            
                      'Model Smooth 10 layers': None,
                      'Model Smooth 20 layers': None,
                      'Model Smooth 30 layers': None,
                      'Response from Smooth 10 layers': None,
                      'Response from Smooth 20 layers': None,
                      'Response from Smooth 30 layers': None,
                      
                      'Model dinoloket layers':None,
                      'Response from dinoloket layers':None,
                      
                      'Model custom layers':None,
                      'Response from custom layers':None,

                      'Model custom smooth layers':None,
                      'Response from custom smooth layers':None,    
                      'Data error':self.error,
                      'Processed AB/2':self.ab2,
                      'Processed RHOA':self.rhoa
                      
                      
                      }
        return

    def blocky_inversion(self,nlay,in_model=0):
        
        
        # check here in case of custom model
        if not np.isscalar(in_model):
            nlay=np.int32((in_model.shape[0]+1)/2)
            if nlay==1:
                sys.exit("ERROR: Only 1 layer detected.") 
            

        
        f  = pg.DC1dModelling(nlay, self.ab2, self.mn2)
        
        
        
        transThk = pg.RTransLog()  # log-transform ensures thk>0
        transRho = pg.RTransLogLU(0.2, 1.1*np.max(self.rhoa))  # lower and upper bound
        transRhoa = pg.RTransLog()  # log transformation for data
        
        
        
        # Blocky Inversion
        # set model transformation for thickness and resistivity
        f.region(0).setTransModel(transThk)  # 0=thickness
        f.region(1).setTransModel(transRho)  # 1=resistivity
        # enerate start model values from median app. resistivity & spread
        paraDepth = max(self.ab2) / 3.  # rule-of-thumb for Wenner/Schlumberger
        f.region(0).setStartValue(paraDepth / nlay / 2)
        f.region(1).setStartValue(np.median(self.rhoa))
        # set up inversion
        inv = pg.RInversion(self.rhoa, f, transRhoa, True)  # data vector, fop, verbose
        # could also be set by inv.setTransData(transRhoa)
        # set error model, regularization strength and Marquardt scheme
        lam = 200.  # (initial) regularization parameter
        errPerc = 1.1*self.error  # relative error of 10 percent
        inv.setRelativeError(errPerc / 100.0)  # alternative: setAbsoluteError in Ohmm
        
        inv.setLambda(lam)  # (initial) regularization parameter
        inv.setMarquardtScheme(0.9)  # decrease lambda by factor 0.9
        
        model = f.createStartVector()  # creates from region start value
        
        if not np.isscalar(in_model):
            # there is a chance that the 1st layer as saved in dinoloket, has thinkness of 0
            for i in range(0,in_model.shape[0]):
                model[i] = in_model[i]  # change default model by cusing the Dinoloket
        else:
            model[0]=1
            
        inv.setModel(model)  #
        # run actual inversion and extract resistivity and thickness
        model = inv.run()  # result is a pg.RVector, but compatible to numpy array

        res, thk = model[nlay-1:nlay*2-1], model[0:nlay-1]
        
        
        # Decrease the regularization (smoothness) and start (from old result)
        # print("inversion with lam=20")
        inv.setLambda(20)
        model2 = inv.run()  # result is a pg.RVector, but compatible to numpy array
        res2, thk2 = model2[nlay-1:nlay*2-1], model2[0:nlay-1]       

        # Optimize now the lam
        model3=inv.runChi1()
        self.res3, self.thk3 = model3[nlay-1:nlay*2-1], model3[0:nlay-1]  
        self.response=np.asarray(inv.response())
        
        # self.inv=inv
        
        # print('rrms={:.2f}%, chi^2={:.3f}'.format(inv.relrms(), inv.chi2()))
            
        # self.block_id=1
        return 
        
    def block(self,nlay,method='x'):
        
        if method=='auto':
            nlay=np.arange(2,9,1)
            
            for k in range(0,len(nlay)):
                self.blocky_inversion(nlay[k])
                thk=np.asarray(self.thk3)
                onder=np.r_[np.cumsum(thk),np.nan]
                boven=np.r_[0,onder[:-1]]
                # keep the inversion results per number of layer

                self.results['Model %d layers'%nlay[k]]=pd.DataFrame(data={'boven':boven,'onder':onder,'Rs':np.asarray(self.res3)})
                self.results['Response from %d layers'%nlay[k]]=np.asarray(self.response)
                
                
                self.block_id=2
        elif method=='Dinoloket':
            nlay=len(self.synres)
           
            in_model=np.r_[self.synthk,self.synres]                        
            self.blocky_inversion(nlay,in_model)
            thk=np.asarray(self.thk3)
            onder=np.r_[np.cumsum(thk),np.nan]
            boven=np.r_[0,onder[:-1]]
            
            
            self.results['Model dinoloket layers']=pd.DataFrame(data={'boven':boven,'onder':onder,'Rs':np.asarray(self.res3)})
            self.results['Response from dinoloket layers']=np.asarray(self.response)
            
        else:
            nlay=nlay
            self.blocky_inversion(nlay)
            self.block_id=1
            thk=np.asarray(self.thk3)
            onder=np.r_[np.cumsum(thk),np.nan]
            boven=np.r_[0,onder[:-1]]
            self.results['Model custom layers']=pd.DataFrame(data={'boven':boven,'onder':onder,'Rs':np.asarray(self.res3)})
            self.results['Response from custom layers']=np.asarray(self.response)
        
        
        
        
        
        return

    def smooth_inv(self,nlay=30):
        
        transThk = pg.RTransLog()  # log-transform ensures thk>0
        transRho = pg.RTransLogLU(0.2, 1.1*np.max(self.rhoa))  # lower and upper bound
        transRhoa = pg.RTransLog()  # log transformation for data
        
        errPerc = 1.1*self.error  # relative error of 3 percent
        
        
        # nlay=30
        # thk = np.logspace(-0.5, 1.0, nlay)
        paraDepth = max(self.ab2) / 3.  # rule-of-thumb for Wenner/Schlumberger
        paraDepth=100
        depth=np.logspace(np.log10(np.min(self.ab2)/3),np.log10(paraDepth),nlay+1)
        thk=depth[1:]-depth[:-1]+np.min(self.ab2)/3
        
        f = pg.DC1dRhoModelling(thk, self.ab2, self.ab2/3)
        inv = pg.RInversion(self.rhoa, f, transRhoa, transRho, False)  # data vector, f, ...
        # The transformations can also be omitted and set individually by
        # inv.setTransData(transRhoa)
        # inv.setTransModel(transRho)
        inv.setRelativeError(errPerc / 100.0)
        # Create a homogeneous starting model
        
        
        model = pg.RVector(len(thk)+1, np.median(self.rhoa))  # uniform values
        inv.setModel(model)  #
        # Set pretty large regularization strength and run inversion
        
        # print("inversion with lam=200")
        inv.setLambda(100)
        res100 = inv.run()  # result is a pg.RVector, but compatible to numpy array
        # print('rrms={:.2f}%, chi^2={:.3f}'.format(inv2.relrms(), inv2.chi2()))
        # Decrease the regularization (smoothness) and start (from old result)
        # print("inversion with lam=20")
        inv.setLambda(10)
        res10 = inv.run()  # result is a pg.RVector, but compatible to numpy array
        # print('rrms={:.2f}%, chi^2={:.3f}'.format(inv2.relrms(), inv2.chi2()))
        # We now optimize lambda such that data are fitted within noise (chi^2=1)
        # print("chi^2=1 optimized inversion")
        resChi = inv.runChi1()  # ends up in a lambda of about 3
        # print("optimized lambda value:", inv.getLambda())
        # print('rrms={:.2f}%, chi^2={:.3f}'.format(inv2.relrms(), inv2.chi2()))        
        
        
        self.res_smooth=np.asarray(resChi)
        self.thk_smooth=thk
       

        self.response=np.asarray(inv.response())
        # print(self.response)
        # print(self.res_smooth)
        self.smooth_id=1
        
        # self.response=inv
        return

    def smooth(self,nlay=30,method='x'):
        if method=='auto':
            self.smooth_inv(10)
            thk=np.asarray(self.thk_smooth)
            onder=np.r_[np.cumsum(thk),np.nan]
            boven=np.r_[0,onder[:-1]]
            # self.smooth_inv,
            # print(thk)
            # print(res_smooth)
            
            self.results['Model Smooth 10 layers']=pd.DataFrame(data={'boven':boven,'onder':onder,'Rs':np.asarray(self.res_smooth)})
            self.results['Response from Smooth 10 layers']=np.asarray(self.response)
            
            self.smooth_inv(20)
            thk=np.asarray(self.thk_smooth)
            onder=np.r_[np.cumsum(thk),np.nan]
            boven=np.r_[0,onder[:-1]]
            # self.smooth_inv,
            self.results['Model Smooth 20 layers']=pd.DataFrame(data={'boven':boven,'onder':onder,'Rs':np.asarray(self.res_smooth)})
            self.results['Response from Smooth 20 layers']=np.asarray(self.response)
            
            self.smooth_inv(30)
            thk=np.asarray(self.thk_smooth)
            onder=np.r_[np.cumsum(thk),np.nan]
            boven=np.r_[0,onder[:-1]]
            # self.smooth_inv,
            self.results['Model Smooth 30 layers']=pd.DataFrame(data={'boven':boven,'onder':onder,'Rs':np.asarray(self.res_smooth)})
            self.results['Response from Smooth 30 layers']=np.asarray(self.response)            
        else:
            self.smooth_inv(nlay)
            thk=np.asarray(self.thk_smooth)
            onder=np.r_[np.cumsum(thk),np.nan]
            boven=np.r_[0,onder[:-1]]
            # self.smooth_inv,
            self.results['Model custom smooth layers']=pd.DataFrame(data={'boven':boven,'onder':onder,'Rs':np.asarray(self.res_smooth)})
            self.results['Response from custom smooth layers']=np.asarray(self.response)
            

        return
        
    def make_plots(self,data,filename):
        
        fig, ax = plt.subplots(ncols=2, figsize=(16, 12))  # two-column figure
        figManager = plt.get_current_fig_manager()
        figManager.window.showMaximized()
        # Dinoloket model shoud exist ???
        
        ab2=data['Raw Data']['L/2'].values
        rhoa=data['Raw Data']['R'].values
        mn2=0.5*np.ones(ab2.shape)
        if  isinstance(data['Inversion Model'][0],pd.DataFrame):
            for k in range(0,len(data['Inversion Model'])):
                synres =list(data['Inversion Model'][k]['Rs'].values)
                synthk=data['Inversion Model'][k]['onder'].values- data['Inversion Model'][k]['boven'].values
                synthk=list(synthk[:-1])
                drawModel1D(ax[0], synthk, synres, plot='semilogx', color='k', label='Dinoloket_%d'%k)
        
        
        
        # Now we loop over the calculated results and plot what ever is stored there
        if 'Results' in data:
            for k in range (2,9):
                if isinstance(data['Results']['Model %d layers'%k],pd.DataFrame):
                    thk=(data['Results']['Model %d layers'%k]['onder']-data['Results']['Model %d layers'%k]['boven']).values
                    res=(data['Results']['Model %d layers'%k]['Rs']).values
                    drawModel1D(ax[0], thk, res, label='Model %d layers'%k)
            if isinstance(data['Results']['Model dinoloket layers'],pd.DataFrame):
                thk=(data['Results']['Model dinoloket layers']['onder']-data['Results']['Model dinoloket layers']['boven']).values
                res=(data['Results']['Model dinoloket layers']['Rs']).values
                drawModel1D(ax[0], thk, res, label='Model dinoloket layers')                   
            for k in range (10,40,10):
                if isinstance(data['Results']['Model Smooth %d layers'%k],pd.DataFrame):
                    thk=(data['Results']['Model Smooth %d layers'%k]['onder']-data['Results']['Model Smooth %d layers'%k]['boven']).values
                    res=(data['Results']['Model Smooth %d layers'%k]['Rs']).values
                    drawModel1D(ax[0], thk, res, label='Model Smooth %d layers'%k)                
            if isinstance(data['Results']['Model custom layers'],pd.DataFrame):
                thk=(data['Results']['Model custom layers']['onder']-data['Results']['Model custom layers']['boven']).values
                res=(data['Results']['Model custom layers']['Rs']).values
                drawModel1D(ax[0], thk, res, label='Model custom layers')                   
            if isinstance(data['Results']['Model custom smooth layers'],pd.DataFrame):
                thk=(data['Results']['Model custom smooth layers']['onder']-data['Results']['Model custom smooth layers']['boven']).values
                res=(data['Results']['Model custom smooth layers']['Rs']).values
                drawModel1D(ax[0], thk, res, label='Model custom smooth layers')  
        


        
        
        ax[0].grid(True, which='both')
        ax[0].set_ylabel('z (m)')
        ax[0].set_xlabel(r'$\rho$ ($\Omega$m)')
        ax[0].legend(loc='best')

        
        
        ax[1].loglog(rhoa, ab2, 'mx-', label='data')  # sounding curve
        ax[1].loglog(data['Results']['Processed RHOA'],data['Results']['Processed AB/2'], 'go-', label='data used')  # sounding curve
        if 'Results' in data:
            for k in range(0,len(data['Results']['Response from Dinoloket'])):
                ax[1].loglog(data['Results']['Response from Dinoloket'][k], data['Results']['Processed AB/2'], 'kx-', label='Response from dinoloket_%d'%k)  # sounding curve
            for k in range (2,9):
                 if isinstance(data['Results']['Response from %d layers'%k],np.ndarray):
                     ax[1].loglog(data['Results']['Response from %d layers'%k], data['Results']['Processed AB/2'], label='Response from %d layers'%k)
            for k in range (10,40,10):
                 if isinstance(data['Results']['Response from Smooth %d layers'%k],np.ndarray):
                     ax[1].loglog(data['Results']['Response from Smooth %d layers'%k], data['Results']['Processed AB/2'], label='Response from Smooth %d layers'%k)              
            if isinstance(data['Results']['Response from dinoloket layers'],np.ndarray):
                ax[1].loglog(data['Results']['Response from dinoloket layers'], data['Results']['Processed AB/2'], label='Response from dinoloket layers')      
            if isinstance(data['Results']['Response from custom smooth layers'],np.ndarray):
                ax[1].loglog(data['Results']['Response from custom smooth layers'], data['Results']['Processed AB/2'], label='Response from custom smooth layers')  
            if isinstance(data['Results']['Response from custom layers'],np.ndarray):
                ax[1].loglog(data['Results']['Response from custom layers'], data['Results']['Processed AB/2'], label='Response from custom layers')                  
            
        ax[1].set_ylim((max(data['Results']['Processed AB/2']), min(data['Results']['Processed AB/2'])))  # downwards according to penetration
        ax[1].grid(True, which='both')
        ax[1].set_xlabel(r'$\rho_a$ ($\Omega$m)')
        ax[1].set_ylabel('AB/2 (m)')
        ax[1].legend(loc='best')
        # plt.show()
        fig.savefig(filename,dpi=100)
                
        
        return fig
        
    def non_uniform_savgol(x, y, window, polynom):
        """
        Applies a Savitzky-Golay filter to y with non-uniform spacing
        as defined in x
    
        This is based on https://dsp.stackexchange.com/questions/1676/savitzky-golay-smoothing-filter-for-not-equally-spaced-data
        The borders are interpolated like scipy.signal.savgol_filter would do
    
        Parameters
        ----------
        x : array_like
            List of floats representing the x values of the data
        y : array_like
            List of floats representing the y values. Must have same length
            as x
        window : int (odd)
            Window length of datapoints. Must be odd and smaller than x
        polynom : int
            The order of polynom used. Must be smaller than the window size
    
        Returns
        -------
        np.array of float
            The smoothed y values
        """
        if len(x) != len(y):
            raise ValueError('"x" and "y" must be of the same size')
    
        if len(x) < window:
            raise ValueError('The data size must be larger than the window size')
    
        if type(window) is not int:
            raise TypeError('"window" must be an integer')
    
        if window % 2 == 0:
            raise ValueError('The "window" must be an odd integer')
    
        if type(polynom) is not int:
            raise TypeError('"polynom" must be an integer')
    
        if polynom >= window:
            raise ValueError('"polynom" must be less than "window"')
    
        half_window = window // 2
        polynom += 1
    
        # Initialize variables
        A = np.empty((window, polynom))     # Matrix
        tA = np.empty((polynom, window))    # Transposed matrix
        t = np.empty(window)                # Local x variables
        y_smoothed = np.full(len(y), np.nan)
    
        # Start smoothing
        for i in range(half_window, len(x) - half_window, 1):
            # Center a window of x values on x[i]
            for j in range(0, window, 1):
                t[j] = x[i + j - half_window] - x[i]
    
            # Create the initial matrix A and its transposed form tA
            for j in range(0, window, 1):
                r = 1.0
                for k in range(0, polynom, 1):
                    A[j, k] = r
                    tA[k, j] = r
                    r *= t[j]
    
            # Multiply the two matrices
            tAA = np.matmul(tA, A)
    
            # Invert the product of the matrices
            tAA = np.linalg.inv(tAA)
    
            # Calculate the pseudoinverse of the design matrix
            coeffs = np.matmul(tAA, tA)
    
            # Calculate c0 which is also the y value for y[i]
            y_smoothed[i] = 0
            for j in range(0, window, 1):
                y_smoothed[i] += coeffs[0, j] * y[i + j - half_window]
    
            # If at the end or beginning, store all coefficients for the polynom
            if i == half_window:
                first_coeffs = np.zeros(polynom)
                for j in range(0, window, 1):
                    for k in range(polynom):
                        first_coeffs[k] += coeffs[k, j] * y[j]
            elif i == len(x) - half_window - 1:
                last_coeffs = np.zeros(polynom)
                for j in range(0, window, 1):
                    for k in range(polynom):
                        last_coeffs[k] += coeffs[k, j] * y[len(y) - window + j]
    
        # Interpolate the result at the left border
        for i in range(0, half_window, 1):
            y_smoothed[i] = 0
            x_i = 1
            for j in range(0, polynom, 1):
                y_smoothed[i] += first_coeffs[j] * x_i
                x_i *= x[i] - x[half_window]
    
        # Interpolate the result at the right border
        for i in range(len(x) - half_window, len(x), 1):
            y_smoothed[i] = 0
            x_i = 1
            for j in range(0, polynom, 1):
                y_smoothed[i] += last_coeffs[j] * x_i
                x_i *= x[i] - x[-half_window - 1]
    
        return y_smoothed