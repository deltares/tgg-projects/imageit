from xml.etree import ElementTree
import geopandas as gpd
from shapely.geometry import Polygon
from zipfile import ZipFile
import requests
import cv2
from io import BytesIO
from itertools import product
import xarray as xr
import numpy as np
import warnings


def ahn_gml_to_gdf(gml_file, ahn="ahn3"):
    """
    Convert gml file of AHN tiles to a GeoDataFrame
    Direct reading using ogr or geopandas did not work for this gml, hence the xml workaround.
    """
    root = ElementTree.parse(gml_file).getroot()
    ns = dict(
        [node for _, node in ElementTree.iterparse(gml_file, events=["start-ns"])]
    )
    featureMembers = root.find("gml:featureMembers", ns)

    bladnrs = []
    geometries = []
    for tile in featureMembers:
        bladnr = tile.find(ahn + ":bladnr", ns).text
        tilegeom = tile.find(ahn + ":geom", ns)
        polygon = tilegeom.find(
            "gml:MultiSurface/gml:surfaceMember/gml:Polygon/gml:exterior/gml:LinearRing/gml:posList",
            ns,
        )
        coordinates = [float(c) for c in polygon.text.split(" ")]

        geometries.append(
            Polygon(
                (
                    (coordinates[0], coordinates[1]),
                    (coordinates[2], coordinates[3]),
                    (coordinates[4], coordinates[5]),
                    (coordinates[6], coordinates[7]),
                    (coordinates[8], coordinates[9]),
                )
            )
        )
        bladnrs.append(bladnr)

    gdf = gpd.GeoDataFrame({"bladnr": bladnrs, "geometry": geometries})

    return gdf


def select_tiles(gdf, bounds):
    """
    Select tiles that are within the bounds. Alternative to the geopands.overlays method previously used,
    which didn't work properly in some cases.
    """
    gdf_bounds = gdf.geometry.bounds
    x_tile_dist = gdf_bounds.loc[0].maxx - gdf_bounds.loc[0].minx
    y_tile_dist = gdf_bounds.loc[0].maxy - gdf_bounds.loc[0].miny

    bound_xmin = bounds[0] - x_tile_dist
    bound_xmax = bounds[1] + x_tile_dist
    bound_ymin = bounds[2] - y_tile_dist
    bound_ymax = bounds[3] + y_tile_dist

    test = [
        all(
            [
                bound.minx >= bound_xmin,
                bound.maxx <= bound_xmax,
                bound.miny >= bound_ymin,
                bound.maxy <= bound_ymax,
            ]
        )
        for _, bound in gdf_bounds.iterrows()
    ]

    return gdf[test]


def download_and_unzip(url, destination_path):
    response = requests.get(url)
    ZipFile(BytesIO(response.content)).extractall(destination_path)


def combine_tiffs(tiffs, outfile, ret=False):
    d_arrays = []
    for tif in tiffs:
        d_arrays.append(xr.open_rasterio(tif).squeeze())
    datasets = [arr.to_dataset(name="arr") for arr in d_arrays]
    mosaic_data = xr.combine_by_coords(datasets, combine_attrs="drop")["arr"]
    mosaic_data.attrs["crs"] = "EPSG:28992"
    mosaic_data = mosaic_data.where(mosaic_data != d_arrays[0].nodatavals[0])
    mosaic_data = mosaic_data.drop_vars("band")
    mosaic_data.attrs["nodatavals"] = np.nan
    mosaic_data["x"].attrs["axis"] = "X"
    mosaic_data["y"].attrs["axis"] = "Y"

    if ret:
        return mosaic_data
    else:
        mosaic_data.to_netcdf(outfile)


def find_best_resolution(xmin, xmax, ymin, ymax, desired_res, max_cells):
    x_cells = (xmax - xmin) / desired_res
    y_cells = (ymax - ymin) / desired_res
    num_cells = x_cells * y_cells

    if num_cells > max_cells:
        reduction_factor = np.sqrt(num_cells / max_cells)
        desired_res * reduction_factor


def find_max_resolution(x_cells, y_cells, desired_res, max_cells):
    """
    Given a maximum amount of cells for e.g. a raster, reduce x_cells and y_cells to the best possible combination.

    x, y _cells: the number of x/y_cells of the original raster
    desired_resolution: resolution to aim for, if possible. If not possible, you will receive a warning
    max_cells: maximum total number of cells
    """
    num_cells = x_cells * y_cells
    orig_x_cells = x_cells

    if num_cells > max_cells:
        reduction_factor = 1 / np.sqrt(num_cells / max_cells)
        ratio = x_cells / y_cells
        x_cells = np.floor(x_cells * reduction_factor)

        # Iteratively find the next possible combination between integers for the reduced resolution
        found = False
        while not found:
            if np.isclose(x_cells / ratio % 1, 0):
                found = True
                y_cells = x_cells / ratio
            else:
                x_cells -= 1

        final_reduction_factor = orig_x_cells / x_cells
        final_resolution = desired_res * final_reduction_factor
        warnings.warn(
            f"Resolution was reduced by a factor {final_reduction_factor} to stay within cell limits. Cell size is now {final_resolution} m"
        )

    return (x_cells, y_cells, final_resolution)


def decode_image_bytestring(img):
    return cv2.imdecode(np.frombuffer(img.read(), np.uint8), -1)


def find_coordinate_labels():
    pass
