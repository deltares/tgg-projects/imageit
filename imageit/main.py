# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 13:31:50 2021

@author: karaouli
"""
# import sys
# sys.path.insert(0,r'.\ahn_reader')
from bro_reader_module import Bro_Reader
from ahn_reader import make_kml_square
from borehole_classification import Borehole_classification
from ahn3_wcs import ahn3_wcs
from geological_models import Geotop, Regis
from surface_models import GenericSurface, GenericSurfaceTimeSeries
from misc_models import ValuePoints
from ves_reader import Ves_reader



def main(Xmin= 130000,Xmax= 138000,Ymin= 441500,Ymax= 446500):
    '''
    

    Parameters
    ----------
    Xmin : TYPE, optional
        DESCRIPTION. The default is 130000.
    Xmax : TYPE, optional
        DESCRIPTION. The default is 138000.
    Ymin : TYPE, optional
        DESCRIPTION. The default is 441500.
    Ymax : TYPE, optional
        DESCRIPTION. The default is 446500.

    Returns
    -------
    None.

    '''
    # Get AHN3 data
    # ahn = GenericSurface.from_downloaded_ahn_tiles(Xmin=Xmin, Xmax=Xmax, Ymin=Ymin, Ymax=Ymax)
    # ahn.to_vtk(filename='AHN3.vtk')

    # # Steering planes
    # st_krwy = GenericSurface.from_raster_file(r'p:\11206817-datagedreven-schematisati\WP 1 - Data and conceptual model\Steering planes\krwy_steering_plane.tif', Xmin=Xmin, Xmax=Xmax, Ymin=Ymin, Ymax=Ymax)
    # st_krwy.to_vtk(filename='steering_plane_krwy.vtk')

    # bathymetrie Lek
    lek = GenericSurface.from_raster_file(r'p:\11206817-datagedreven-schematisati\WP 1 - Data and conceptual model\Diepteligging_Lek\ON_zuid_NAP.tif', Xmin=Xmin, Xmax=Xmax, Ymin=Ymin, Ymax=Ymax)
    #Xmin=584000, Xmax=600000, Ymin=5800000, Ymax=5817000
    #, Xmin=76000, Xmax=93000, Ymin=484000, Ymax=502000)
    lek.to_vtk(filename='bathymetry_lek.vtk')

    # # Get bathy (NOT KVK-RELATED!)
    # bath = GenericSurfaceTimeSeries.from_netcdf(r'p:\1204421-kpp-benokust\2021\3-MorfologieKust\3A1 - Geologie\Kaart\experimenteel\Borndiep\Borndiep_interp4.nc', time_axis='year')
    # bath.to_vtk(base_filename='borndiep_bathy', bottom=-30)


    # # Process InSar data and project it on AHN.
    # insar = ValuePoints.from_vectorfile(r'p:\11206817-datagedreven-schematisati\WP 1 - Data and conceptual model\InSar\buk_sensar_clipped.gpkg', 'VELOCITY', elevation=ahn, desired_crs=28992)
    # insar.to_pointcloud()
    # insar.save_vtk(r'p:\11206817-datagedreven-schematisati\WP 1 - Data and conceptual model\VTK Results\InSar.vtk')

    # # Initialize the Bro module
    # # cpts = Bro_Reader(Xmin=Xmin,Xmax=Xmax,Ymin=Ymin,Ymax=Ymax)
    # # cpts.read_cpt()
    # # cpts.plot_cpts()
    
    # # make_kml_square(Xmin=Xmin,Ymin=Ymin,Xmax=Xmax,Ymax=Ymax)
    
    # Initialize the Geotop module
    geotop = Geotop.from_netcdf(r'p:\430-tgg-data\Geotop\Geotop14\geotop.nc') #, Xmin=Xmin,Xmax=Xmax,Ymin=Ymin,Ymax=Ymax)
    # geotop = Geotop.from_layers(r'p:\430-tgg-data\Geotop\Geotop14\GTP14\GeoTOP_v01r4\GeoTOP_v01r4\voxelmodel\GeoTOP_v01r4_doorsnede_lithoklasse_NAP\grids', 
    #               Xmin=Xmin,Xmax=Xmax,Ymin=Ymin,Ymax=Ymax)
    geotop.to_vtk(package='pyvista')
    
    
    # # Initiallize the Boreholes
    # # bh = Borehole_classification()
    # # bh.select_boreholes(Xmin=Xmin,Ymin=Ymin,Xmax=Xmax,Ymax=Ymax)
    # # bh.plot_boreholes()
    
    #Initiallize the Regis data
    regis = Regis.from_netcdf(r'p:\430-tgg-data\REGIS II\REGIS.nc', Xmin=Xmin,Xmax=Xmax,Ymin=Ymin,Ymax=Ymax)
    
    # Example: Export horizontal conductivity of a singe layer (Kreftenheye zandige eenheid 2)
    # name, top, bottom, data = regis.get_single_layer(b'KRz2', data_vars=('kh',))
    # regis.to_vtk(name, top, bottom, data)

    # Example: export all REGIS layers down to -60 m NAP. 
    # Fill values will be codes related to REGIS geological units
    # layer_generator = regis.get_all_layers(max_depth=-60)
    # regis.to_vtk_all(layer_generator, binary=True)

    # Example: export vertical and horizontal hydraulic conductivity for all REGIS layers down to -60 m NAP 
    layer_generator_k = regis.get_all_layers(('kh', 'kv'), max_depth=-60)
    regis.to_vtk_all(layer_generator_k, separate_files=False)
    

    # 

    # # Example: you can burn values of another raster to the surface and extend the bottom of the model
    # # such that it becomes a solid
    #ahn2 = AHN.from_tiles(Xmin=Xmin,Xmax=Xmax,Ymin=Ymin,Ymax=Ymax)
    #ahn.to_vtk(project=ahn2, bottom=-50)

    #mosaic,out_trans=ahn3_wcs(rBBOX=[Xmin,Ymin,Xmax,Ymax],ahnid = str('ahn3_5m_dtm'))
    
    
    # Get VES Data
    int4=Ves_reader()
    int4.select_ves(Xmin=Xmin,Ymin=Ymin,Xmax=Xmax,Ymax=Ymax)
    int4.plot_ves()
    





if __name__ == "__main__":
    Xmin= 130000
    Xmax= 138000
    Ymin= 441500
    Ymax= 446500
    main(Xmin= Xmin,Xmax= Xmax,Ymin= Ymin,Ymax= Ymax)

    




