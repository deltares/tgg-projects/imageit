# -*- coding: utf-8 -*-
"""
Created on Wed Oct 13 08:06:59 2021

@author: karaouli
"""

from vtk import * 
import glob


reader = vtkPolyDataReader()
append = vtkAppendPolyData()

filenames = glob.glob('*.vtk')
for file in filenames:
    reader.SetFileName(file)
    reader.Update()
    polydata = vtkPolyData()
    polydata.ShallowCopy(reader.GetOutput())
    append.AddInputData(polydata)

append.Update()    

writer = vtkPolyDataWriter()
writer.SetFileName('output.vtk')
writer.SetInput(append.GetOutput())
writer.Write()