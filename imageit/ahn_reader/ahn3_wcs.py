# import the required libraries
import math
import urllib.request
import rasterio
from rasterio.merge import merge
from rasterio.plot import show
import os
import sys
sys.path.insert(0, r'..\geo_vtk\src')
from vtkclass import VtkClass
import shutil


#-----setting the output location & choose type (dsm/dtm) ------
# Set an output location were the ahn tiles will be stored
def ahn3_wcs(rBBOX=[ 57639.9,405019.4,59892.5,415182.8 ], ahnid = str('ahn3_5m_dtm'),savefile=0,plot=1):
    if os.path.isdir(os.getcwd()+'\\temp'):
        print('Saving temp files....\n')
    else:
        os.mkdir('temp')
        print('Making temp subfolder. Saving temp files....\n')
    
    Uitvoermap=os.getcwd()+'\\temp'
    #Uitvoermap = r'D:\karaouli\Desktop\Projects\et_proposal\from_zitman\temp'                                
    # Select the product that you want: 'ahn3_05m_dtm','ahn3_05m_dsm','ahn3_5m_dsm','ahn3_5m_dtm'                       
#    ahnid = str('ahn3_05m_dtm')
    # Making an empty list were the filenames can be stored 
    bestanden = [] 
    # The coordinates should be in RD new (EPSG:28992) [xmin,ymin,xmax,ymax]                                                        
#    rBBOX=[ 57639.9,405019.4,59892.5,415182.8    ]                                    
    
    #----- Coordinates are rounded on 5 meter-----
    x1=int(5*int(rBBOX[0]/5))-5
    x2=int(5*int(rBBOX[2]/5))+5
    y1=int(5*int(rBBOX[1]/5))-5
    y2=int(5*int(rBBOX[3]/5))+5
    
    #-----Variabelen-----
    max_afmeting = 2000.0                                                           # maximum permitted size for downloading
    breedte = x2-x1                                                                 # Getting the width
    hoogte = y2-y1                                                                  # Getting the height
    aantal_breedte = math.ceil(breedte / max_afmeting)                              # Calculate how many tiles we have to request
    aantal_hoogte = math.ceil(hoogte / max_afmeting)                                # Calculate how many tiles we have to request
    print('Downloading %d files from PDOK\n'%(aantal_breedte*aantal_hoogte))
    #----- request the tile(s) -----
    for r in range(1, aantal_breedte+1):
        for p in range(1, aantal_hoogte+1):
            if r < aantal_breedte and p < aantal_hoogte:
                bbox=str(x1+(r-1)*max_afmeting)+","+str(y1+(p-1)*max_afmeting)+","+str(x1+r*max_afmeting)+","+str(y1+p*max_afmeting)
            if r == aantal_breedte and p < aantal_hoogte:
                resterend_deel_breedte = breedte - ((aantal_breedte - 1) * max_afmeting)
                bbox=str(x1+(r-1)*max_afmeting)+","+str(y1+(p-1)*max_afmeting)+","+str(x1+(r-1)*max_afmeting+resterend_deel_breedte)+","+str(y1+p*max_afmeting)
            if r < aantal_breedte and p == aantal_hoogte:
                resterend_deel_hoogte = hoogte - ((aantal_hoogte - 1) * max_afmeting)
                bbox=str(x1+(r-1)*max_afmeting)+","+str(y1+(p-1)*max_afmeting)+","+str(x1+r*max_afmeting)+","+str(y1+(p-1)*max_afmeting+resterend_deel_hoogte)
            if r == aantal_breedte and p == aantal_hoogte:
                resterend_deel_breedte = breedte - ((aantal_breedte - 1) * max_afmeting)
                resterend_deel_hoogte = hoogte - ((aantal_hoogte - 1) * max_afmeting)
                bbox=str(x1+(r-1)*max_afmeting)+","+str(y1+(p-1)*max_afmeting)+","+str(x1+(r-1)*max_afmeting+resterend_deel_breedte)+","+str(y1+(p-1)*max_afmeting+resterend_deel_hoogte)

            if ahnid == 'ahn3_5m_dsm' or ahnid == 'ahn3_5m_dtm':
               res = str("resx=5&resy=5")
            else:
               res = str("resx=.5&resy=.5")

            #-----AHN3 0.5m maaiveld raster downloaden uit WCS-----
            URI = "https://geodata.nationaalgeoregister.nl/ahn3/wcs?service=WCS&version=1.0.0&crs=EPSG:28992&request=GetCoverage&coverage="+ahnid+"&BBox="+bbox+"&format=GEOTIFF_FLOAT32&"+res
            contents = urllib.request.urlretrieve(URI, Uitvoermap+ "\\" + ahnid+str(r)+str(p)+".tif")
            content_type = contents[1]["Content-Type"]
            bestanden.append(Uitvoermap+ "\\" + ahnid +str(r)+str(p)+".tif") #Bestandnaam toevoegen aan de lijst met bestandnamen
            
    # open the downloaded rasters in python.
    src_files_to_mosaic = []
    for x in bestanden:
        src = rasterio.open(x)
        src_files_to_mosaic.append(src)
    
    # Merge function returns a single mosaic array and the transformation info
    mosaic, out_trans = merge(src_files_to_mosaic)
    
    # Copy the metadata
    out_meta = src.meta.copy()
    
    # Update the metadata
    out_meta.update({"driver": "GTiff",
                     "height": mosaic.shape[1],
                     "width": mosaic.shape[2],
                     "transform": out_trans
                     })
        
    # Write the mosaic raster to disk
    merged_output = Uitvoermap + "\\" + ahnid + "_merge.tif"
    with rasterio.open(merged_output, "w", **out_meta) as dest:
        dest.write(mosaic)
    
    for i in range(len(src_files_to_mosaic)):
        src_files_to_mosaic[i].close()
        print(src_files_to_mosaic[i])
    
    for i in range(len(src_files_to_mosaic)):
        os.remove(src_files_to_mosaic[i].name)
    
        
    # if plot==1:
    #     int1=VtkClass()
    #     int1.tiff_to_vtk(merged_output)
    #     #move file to /data vtk
    #     shutil.move(merged_output[:-4]+'_2d.vtk', 'results\\vtk\\ahn3.vtk')

    return mosaic, out_trans    