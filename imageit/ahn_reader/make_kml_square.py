# -*- coding: utf-8 -*-
"""
Created on Fri Oct  8 08:36:21 2021

@author: karaouli
"""

import simplekml
import pyproj
import numpy as np




def make_kml_square(Xmin=0,Ymin=0,Xmax=0,Ymax=0):
    
    
    #make now for corner pair
    pair=np.array([[Xmin,Ymin],
                   [Xmax,Ymin],
                   [Xmax,Ymax],
                   [Xmin,Ymax],
                   [Xmin,Ymin]
                   ])
    
    
    rd_p = pyproj.Proj(init='epsg:28992') 
    utm_xy_31 = pyproj.Proj(init='epsg:32631') 
    wgs84_p = pyproj.Proj(init='epsg:4326')
    
    
    # xxx,yyy=pyproj.transform(rd_p,wgs84_p,pair[:,0],pair[:,1])
    
    # kml=simplekml.Kml()
    # for nn in range(0,len(xxx)):
    # #    x[nn], y[nn] = tilemapbase.project(*cc[nn])
    #     kml.newpoint(name=np.str(nn), coords=[(xxx[nn],yyy[nn])])
    kml=simplekml.Kml()
    for i in range(0,len(pair)-1):
        lat1,lon1=pyproj.transform(rd_p,wgs84_p,pair[i,0],pair[i,1])
        lat2,lon2=pyproj.transform(rd_p,wgs84_p,pair[i+1,0],pair[i+1,1])
        lin=kml.newlinestring(name='Line %d'%(i+1),description='Area',
                              coords=[(lat1,lon1),(lat2,lon2)])
    
    kml.save('square.kml')    
   