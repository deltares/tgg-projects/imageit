import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
from .make_kml_square import make_kml_square
from .ahn3_wcs import ahn3_wcs

__version__ = "0.6.0.0"