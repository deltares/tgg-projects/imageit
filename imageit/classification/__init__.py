import os
import sys

sys.path.append(os.path.dirname(os.path.realpath(__file__)))
from .translate import LithoTranslate


__version__ = "0.6.0.0"
