# from abc import ABC, abstractmethod
from pathlib import Path
import numpy as np
import pyvista as pv
import xarray as xr
from dataclasses import dataclass
from typing import List


@dataclass(repr=False, eq=False)
class Raster(object):
    """
    Abstract base class for all raster datasets. Attribute 'model' must be an Xarray.DataArray or DataSet
    with at least coordinates 'x' and 'y'.

    self.model = The Xarray.DataArray or DataSet representation of the model
    self.pv_model = The PyVista Structured/Uniform/Unstructured grid representation of the model
    """

    def __new__(cls, *args, **kwargs):
        if cls is Raster:
            raise TypeError(
                f"Cannot construct '{cls.__name__}' directly: construct class from its children instead"
            )
        else:
            return object.__new__(cls)

    def __init__(self, source, model):
        self.source = source
        self.model = model
        self.pv_model = None

    def __repr__(self):
        return f"{self.__class__.__name__}:\nsource = {self.source}\ndx = {self.dx}\ndy = {self.dy}\nXmin = {self.Xmin}\nXmax = {self.Xmax}\nYmin = {self.Ymin}\nYmax = {self.Ymax}"

    def __eq__(self, other):
        """
        when evaluating raster_object1==raster_object2, return True when
        the x and y coordinate arrays of the two instances are identical.
        """
        return all(((self.xc == other.xc).all(), (self.yc == other.yc).all()))

    @property
    def xc(self):
        """
        Get the x coordinates as np.array
        """
        return self.model.x.values

    @property
    def yc(self):
        """
        Get the y coordinates as np.array
        """
        return self.model.y.values

    @property
    def dx(self):
        """
        delta x. Resolution along x-axis
        """
        return self.xc[1] - self.xc[0]

    @property
    def dy(self):
        """
        delta y. Resolution along y-axis
        """
        return self.yc[1] - self.yc[0]

    @property
    def Xmin(self):
        """
        Property Xmin derived from model. Since properties can't be explicitly set,
        this always represents the correct Xmin value for the current model.
        """
        return self.xc.min()

    @property
    def Xmax(self):
        """
        Property Xmin derived from model. Since properties can't be explicitly set,
        this always represents the correct Xmax value for the current model.
        """
        return self.xc.max()

    @property
    def Ymin(self):
        """
        Property Xmin derived from model. Since properties can't be explicitly set,
        this always represents the correct Ymin value for the current model.
        """
        return self.yc.min()

    @property
    def Ymax(self):
        """
        Property Xmin derived from model. Since properties can't be explicitly set,
        this always represents the correct Ymax value for the current model.
        """
        return self.yc.max()

    def model_as_array(self, data_var=""):
        """
        Get model values as np.array for use in geovtk or pyvista
        """
        if "data_vars" in self.__dict__.keys() and data_var != "":
            return self.model[data_var].values
        elif "data_vars" in self.__dict__.keys():
            return self.model[self.data_vars[0]].values
        else:
            return self.model.values

    def clip_ds(self, model_ds, *args, data_vars=[], ret=False, **kwargs):
        """
        Method to clip a raster model (represented by an xr.DataSet)

        *args are in order: Xmin, Xmax, Ymin, Ymax
        possible **kwarg = shapefile when a polygon shapefile is to be used for clipping

        """

        if len(args) == 4 and all([isinstance(c, (float, int)) for c in args]):
            model = model_ds.sel(x=slice(args[0], args[1]), y=slice(args[2], args[3]))
        else:
            try:
                kwargs["shapefile"]
                raise NotImplementedError("Shapefile clipping is not yet supported")
            except KeyError:
                raise NotImplementedError("Shapefile clipping is not yet supported")

        if isinstance(model_ds, xr.Dataset):
            if all([d in list(model.data_vars) for d in data_vars]):
                model = model[data_vars]
            elif len(data_vars) != 0:
                print("Given data var is not an existing data var in the xr.Dataset")

        if ret:
            return model
        else:
            self.model = model

    def create_basic_structured_grid(self, array=None):
        x, y = np.meshgrid(np.float32(self.xc), np.float32(self.yc))
        if array is None:
            return pv.StructuredGrid(x, y, self.model_as_array())
        else:
            return pv.StructuredGrid(x, y, array)

    def reproject(self, current_crs, new_crs):
        """
        TODO: Function to transform raster crs to another one. Maybe plug-in Imod-Python function here
        """
        pass

    def to_netcdf(self, file):
        """
        export DataArray or Dataset to NetCDF file
        """
        self.model.to_netcdf(file)

    @staticmethod
    def clip_da(model_da, Xmin, Xmax, Ymin, Ymax):
        return model_da.sel(x=slice(Xmin, Xmax), y=slice(Ymin, Ymax))

    def save_vtk(self, file, binary=True):
        self.pv_model.save(file, binary=binary)


@dataclass(repr=False)
class PointCollection(object):
    """
    TODO

    WIP

    Abstract base class for a collection of point data, such as boreholes, cpt's and insar points.

    self.model = Pandas.Series with model values
    self.value_field = String with name of column in the soruce data to be used for self.model
    self.geometry = Series with Shapely geometry objects
    self.elevation = array with elevation values for each point
    self.pv_model = PyVista representation of the model
    """

    def __new__(cls, *args, **kwargs):
        if cls is PointCollection:
            raise TypeError(
                f"Cannot construct {cls.__name__} directly: construct class from its children instead"
            )
        else:
            return object.__new__(cls)

    def __init__(self, source, model, value_field, geometry, elevation):
        self.source = source
        self.model = model
        self.pv_model = None
        self.value_field = value_field
        self.geometry = geometry
        self.elevation = elevation

    def __repr__(self):
        return f"{self.__class__.__name__}:\nsource = {self.source.name}"

    @property
    def crs(self):
        return self.model.crs

    @property
    def geometry_as_array(self):
        """
        Turn the geometry Geoseries into an n x 2 numpy array.
        """
        if type(self.geometry) != np.ndarray:
            return np.array(
                list(
                    map(
                        lambda c: [
                            float(i) for i in c.wkt.split("(")[1].strip(")").split(" ")
                        ],
                        self.geometry,
                    )
                )
            )
        else:
            return self.geometry

    def clip(self):
        """
        Method to clip points and throw away those that lie outside of given bounds
        """
        pass

    def set_elevation(self, valueraster_object):
        """
        Update self.elevation of points based on a surface_models.surface_model_base.ValueRaster object
        """
        if isinstance(valueraster_object, Raster):
            geom_arr = self.geometry_as_array
            elevation_model = valueraster_object.model

            for i in range(len(self.elevation)):
                self.elevation[i] = elevation_model.sel(
                    x=geom_arr[i, 0], y=geom_arr[i, 1], method="nearest"
                ).values

    def reproject(self, new_crs):
        """
        Reproject points to a different CRS, given by its EPSG number (as integer)
        """
        self.geometry = self.geometry.to_crs(new_crs)

    def save_vtk(self, file, binary=True):
        self.pv_model.save(file, binary=binary)


class LineSlices(object):
    """
    TODO

    NOT IMPLEMENTED

    FUTURE Abstract base class for a collection of data along 2D lines (slices).
    e.g. EM or ERT data that was acquired along a line.

    self.model is the only mutable attribute in this class. The rest are properties that
    are derived from the model dataset.
    """

    def __init__(self, source, model, pv_model):
        self.source = source
        self.model = model
        self.pv_model = pv_model

    def __repr__(self):
        return f"{self.__class__.__name__}"

    def save_vtk(self, file, binary=True):
        self.pv_model.save(file, binary=binary)
