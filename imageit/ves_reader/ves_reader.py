# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 14:46:33 2019

@author: karaouli
"""

import glob
import pandas as pd
import numpy as np
import re
from io import StringIO
from pathlib import Path
import pickle
from matplotlib.path import Path as PPath
import sys
sys.path.insert(0, r'.\geo_vtk\src')
from vtkclass import VtkClass


class Ves_reader():
    def __init__(self,filename=r'data/Geo-electrisch onderzoek/'):
        
        """        
        This package reads the txt file with ves (from BRO), makes a selection based on the polygon provided,
        and plots all VES in VTK format (to be viewed with Paraview)
        All files will be stored to the results\data for the selected file (pickle fomrat)
        and results\vtk\ves.vtk for the graphics output. 
        
        Parameters
        ----------          
        :datapath Provide path to the txt files
        :polygon Fiona shapefile. 
        :Xmin
        :Xmax
        :Ymin
        :Ymax     
        Returns
        -------
        None.        
        """
        
        
        self.filename=filename
        self.read_raw_data()

        
        
        return
    
    def read_raw_data(self):
        
        yt=glob.glob(self.filename+'\*.txt')
        
        # search is pickle already found
        
        ves=Path(self.filename+'ves.p')
        if ves.is_file():
            with open(self.filename+'ves.p', 'rb') as f:
                self.ves = pickle.load(f)        
        else:
            print('Reading files....')
            self.ves=dict()
            for i in range(0,len(yt)):
                tmp=self.read_txt(yt[i])
                self.ves[tmp['ID']]= {'Coordinate System':tmp['Coordinate System'],'Coordinates':tmp['Coordinates'],'Elevation':tmp['Elevation'],'Raw Data':tmp['Raw data'],'Inversion Model':tmp['Inversion Model']}
                
        return
        
    def read_txt(self,filename):
        # Read individual files and return into a dict
        a=open(filename,'r')
        lines=a.readlines()
        a.close()
        
        # get name
        id_name=re.sub(';\n','',lines[0])
        coordinate_sytem=re.sub(';\n','',lines[1])
        
        coordinates=np.fromstring(re.sub(';',' ',lines[2]),sep=',')
        elevation=np.fromstring(re.sub(';',' ',lines[3]),sep=',')
        
        
        # find sapcesd
        
        # search terms below for data
        ix_up=np.where( np.char.strip(lines)=='VES data;')[0]
        
        ix_down=np.where( np.char.strip(lines)=='Kop VES interpretatie;')[0]
        str1=" "
        str1=str1.join(lines[ix_up[0]+1:ix_down[0]])
        raw_data=pd.read_csv(StringIO(str1),sep=';')
        
        # Search if interpration exist
        ix_up=np.where( np.char.strip(lines)=='VES interpretatie;')[0]
        if ix_up.shape[0]>0:
            ix_down = np.int32(np.zeros(ix_up.shape))
            for k in range(0,ix_up.shape[0]):
                # find next ;
                for n in range(ix_up[k],len(lines)):
                    if (lines[n])==';\n':
                        ix_down[k]=n
                        break

            if ix_down[-1]==0:
                ix_down[-1]=len(lines)
            inversion={}
            for k in range(0,ix_up.shape[0]):
                str1=" "
                str1=str1.join(lines[ix_up[k]+1:ix_down[k]])
                inversion[k]=pd.read_csv(StringIO(str1),sep=';')


        else:
            inversion=0
        #make dictionary
        out={'ID':id_name,
             'Coordinate System':coordinate_sytem,
             'Coordinates':coordinates,
             'Elevation':elevation,
             'Raw data':raw_data,
             'Inversion Model':inversion
             }
        
        return out

    def select_ves(self,Xmin=-np.Inf,Ymin=-np.Inf,Xmax=np.Inf,Ymax=np.Inf):
        
        p=PPath(np.array([[Xmin,Ymin],
                            [Xmax,Ymin],
                            [Xmax,Ymax],
                            [Xmin,Ymax],
                            [Xmin,Ymin] ]))  
        
        
        keys=list(self.ves.keys())    
        xx=np.zeros((len(keys),2))
        for i in range(0,len(keys)):
            xx[i,:]=self.ves[keys[i]]['Coordinates']
        
        flags =p.contains_points(xx)
        flags=np.where(flags)[0]
        self.sel_ves = [self.ves[keys[i]] for i in flags]
        self.sel_loc= [keys[i] for i in flags]


    def plot_ves(self,radius=20):
        intvv=VtkClass()
        t1=0  # flag to make matrix first time
        for i in range(0,len(self.sel_ves)):
            
            data=self.sel_ves[i]['Inversion Model'][0]
            if isinstance(data, pd.DataFrame):
                data=data[['boven','onder','Rs']].values
                    
                tmp_loc=self.sel_ves[i]['Coordinates']
                tmp_elev=self.sel_ves[i]['Elevation']
                tmp_loc=np.c_[tmp_loc[0],tmp_loc[1],tmp_elev]
                tmp_loc=np.tile(tmp_loc, (data.shape[0],1))
                data=np.c_[data[:,:2],tmp_loc,data[:,2]]
                data[-1,1]=1.2*data[-1,0] # put somethoing for half space
                
                if t1==0:
                    out=data
                    t1=1
                else:
                    out=np.r_[out,data]
        
        intvv.make_borehole_as_cube_multi('results\\vtk\\ves.vtk',out,radius,label=['Resistivity_Ohm.m'])



        