from base import PointCollection, Raster  # pylint: disable=import-error
import download  # pylint: disable=import-error
import geopandas as gpd
import pandas as pd
import numpy as np
import pyvista as pv
from pathlib import Path


class ValuePoints(PointCollection):
    def __init__(self, model, source, value_field, geometry, elevation, desired_crs):
        super().__init__(source, model, value_field, geometry, None)
        # self.reproject(desired_crs)

        # Initialise elevation, either zero or based on an instance of Surface
        self.elevation = np.zeros([len(self.model), 1])
        if isinstance(elevation, Raster):
            self.set_elevation(elevation)

    @classmethod
    def from_vectorfile(
        cls, vectorfile, value_field_name, elevation=None, desired_crs=28992
    ) -> PointCollection:
        source = Path(vectorfile)
        gdf = gpd.read_file(source)
        model = gdf[value_field_name]
        geometry = gdf.geometry
        return cls(model, source, value_field_name, geometry, elevation, desired_crs)

    @classmethod
    def from_xyzfile(
        cls, xyz_file, elevation=None, desired_crs=28992
    ) -> PointCollection:
        source = Path(xyz_file)
        df = pd.read_csv(xyz_file, header=None, names=["x", "y", "z"])
        model = df["z"]
        geometry = df[["x", "y"]].values
        return cls(model, source, "z", geometry, elevation, desired_crs)

    @property
    def model_as_array_with_values_as_z(self):
        return np.hstack(
            (self.geometry_as_array, np.array([self.model.values]).transpose())
        )

    @property
    def model_as_array_with_elevation_as_z(self):
        return np.hstack((self.geometry_as_array, self.elevation))

    def to_pointcloud(self, z_source="values", reconstruct_surface=False):
        if z_source == "values":
            self.pv_model = pv.PolyData(self.model_as_array_with_values_as_z)
        elif z_source == "elevation":
            self.pv_model = pv.PolyData(self.model_as_array_with_elevation_as_z)
            self.pv_model[self.value_field] = self.model_as_array_with_values_as_z[:, 2]
        else:
            raise Exception("Unknown z_source")

        if reconstruct_surface:
            self.pv_model = self.pv_model.reconstruct_surface()

    def to_vtk(
        self,
        file=None,
        binary=True,
        z_source="values",
        reconstruct_surface=False,
        method="pyvista",
    ):
        """
        z_source can be values: the height of the points is determined by the value of each point
        if z_source = elevation: the height of points is determined by a raster that was initialised as self.elevation
        """
        if file is None:
            file = (
                Path(__file__)
                .parents[2]
                .joinpath(
                    Path(f"results\\vtk\\" + f"{self.__class__.__name__}" + ".vtk")
                )
            )
        if method == "pyvista":
            self.to_pointcloud(
                z_source=z_source, reconstruct_surface=reconstruct_surface
            )
            self.save_vtk(file, binary=binary)
        elif method == "geovtk":
            pass


class Texture(object):
    """
    Base class for simple images that can be used as a texture
    This is e.g. sattellite imagery
    """

    def __init__(self, image, xmin, xmax, ymin, ymax):
        self.image = image
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax

    @classmethod
    def from_wms(
        cls,
        wms_location="https://service.pdok.nl/hwh/luchtfotorgb/wms/v1_0?request=GetCapabilities&service=wms",
        wms_feature="Actueel_ortho25",
        xmin=0,
        xmax=10000,
        ymin=400000,
        ymax=410000,
        **kwargs,
    ):
        image = download.WMS_download(wms_location, wms_feature, xmin, xmax, ymin, ymax)

        return cls(image, xmin, xmax, ymin, ymax)
