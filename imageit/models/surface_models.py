from base import Raster  # pylint: disable=import-error
import download as dwn  # pylint: disable=import-error
import numpy as np
import xarray as xr
from pathlib import Path


class Surface(Raster):
    """
    Class for Surfaces. A Surface is an Xarray.DataArray with coordinates
    'x' and 'y' and contains integer or float values. e.g. the AHN DTM is a Surface.
    """

    def __init__(self, source, model):
        super().__init__(source, model)

    @property
    def min_value(self):
        return self.model.min()

    @property
    def max_value(self):
        return self.model.max()

    @classmethod
    def from_downloaded_ahn_tiles(
        cls,
        *tile_location,
        download=True,
        Xmin=-np.Inf,
        Xmax=np.Inf,
        Ymin=-np.Inf,
        Ymax=np.Inf,
        **kwargs,
    ):
        if len(tile_location) > 1:
            raise Exception("More than one tile location was given")

        if download:
            dwn.AHN_kaartblad(**kwargs)
            ds = dwn.AHN_tiles((Xmin, Xmax, Ymin, Ymax), return_result=True, **kwargs)

        model = cls.clip_ds(cls, ds, Xmin, Xmax, Ymax, Ymin, ret=True)

        return cls("Downloaded AHN Tiles", model)

    @classmethod
    def from_local_ahn_tiles(cls):
        """
        Mosaic local AHN tiles and construct Surface from this
        """

        pass

    @classmethod
    def from_raster_file(
        cls,
        file,
        band=1,
        Xmin=-np.Inf,
        Xmax=np.Inf,
        Ymin=-np.Inf,
        Ymax=np.Inf,
        **kwargs,
    ):
        """
        Construct Surface from a raster file, like tiff or asc

        Uses Xarray.from_rasterio method
        """
        source = Path(file)
        model = xr.open_rasterio(source)
        model = model.sel(band=band)
        model = cls.clip_ds(cls, model, Xmin, Xmax, Ymax, Ymin, ret=True)
        model = model.where(model != model.nodatavals[0])
        model.attrs["nodatavals"] = np.nan

        return cls(source, model)

    @classmethod
    def from_polygon_layer(cls, file, like):
        """
        Rasterize a polygon according to 'like'. Like is another instance of a Surface class.
        Uses the Imod-Python function
        """
        pass

    def to_vtk(
        self,
        file=None,
        project=None,
        texture=None,
        bottom=None,
        binary=True,
        method="pyvista",
    ):
        """
        Export a Surface, such as e.g. the AHN DTM, to vtk

        kwargs:
        project = another Surface or RGBSurface object to project on the model
                In the future, use this to project e.g. a satellite image on the AHN surface
        bottom = Arbitrary bottom level to turn the surface into a block. Value must be lower
        than the minimum raster value of self.model.
        binary = export vtk as binary (True) of ASCII (False)
        mehtod = Export with pyvista or geovtk

        """
        if file is None:
            file = (
                Path(__file__)
                .parents[2]
                .joinpath(
                    Path(f"results\\vtk\\" + f"{self.__class__.__name__}" + ".vtk")
                )
            )
        if method == "pyvista":
            self.__to_vtk_pyvista(
                file, project=project, texture=texture, bottom=bottom, binary=binary
            )
        elif method == "geovtk":
            self.__to_vtk_geovtk(
                file,
                project=project,
                bottom=bottom,
            )

    def __to_vtk_geovtk(self, file, project=None, bottom=None):
        pass

    def __to_vtk_pyvista(
        self, file, project=None, texture=None, bottom=None, binary=True
    ):
        """
        Export a Surface, such as e.g. the AHN DTM, to vtk

        kwargs:
        project = another Surface or RGBSurface object to project on the model
                In the future, use this to project e.g. a satellite image on the AHN surface
        bottom = Arbitrary bottom level to turn the surface into a block. Value must be lower
        than the minimum raster value of self.model.
        binary = export vtk as binary (True) of ASCII (False)

        """
        self.pv_model = self.create_basic_structured_grid()
        projected_data = self.__project(project)

        if isinstance(bottom, (float, int)):
            if bottom < self.min_value:
                pv_top = self.pv_model.points.copy()
                pv_bottom = self.pv_model.points.copy()
                pv_bottom[:, -1] = bottom
                self.pv_model.points = np.vstack((pv_top, pv_bottom))
                self.pv_model.dimensions = [*self.pv_model.dimensions[0:2], 2]
                data_flat = projected_data.flatten(order="F")
                self.pv_model.point_data["values"] = np.hstack((data_flat, data_flat))
            else:
                self.pv_model.point_data["values"] = projected_data.flatten(order="F")
                raise Warning(
                    "Given bottom value lies above or is equal to minimum height of surface"
                )
        else:
            self.pv_model.point_data["values"] = projected_data.flatten(order="F")

        # To pv.UnstructuredGrid by removing NaN cells from the StructuredGrid
        self.pv_model = self.pv_model.threshold()
        self.save_vtk(file, binary=binary)

    def __project(self, project):
        if project is None:
            project = self
        elif self == project:
            # if self is 'equal' to project (see abc method __eq__ for conditions)
            # 'project' is valid for projecting its values onto self, so continue.
            pass
        elif self != project:
            raise IndexError(
                f"The instance of '{project.__class__.__name__}' cannot project its model values onto self because the coordinates do not match!\nMake sure that '{self.__class__.__name__}' is also an instance of Raster class with identical x and y-coordinates"
            )
        else:
            # TODO: there are probably some more exceptions where this will raise an error.
            raise Exception(f"Couldn't project values onto self for unknown reason")

        return project.model_as_array()

    def resample_like_other(self, other):
        pass


class SurfaceTimeSeries(Raster):
    """
    Class for a collection of Surfaces, e.g. a timeseries of surfaces.
    """

    def __init__(self, source, model, time_axis):
        super().__init__(source, model)
        self.time_axis = time_axis

    @property
    def timesteps(self):
        return self.model[self.time_axis]

    @classmethod
    def from_netcdf(
        cls,
        nc_file,
        time_axis="year",
        Xmin=-np.Inf,
        Xmax=np.Inf,
        Ymin=-np.Inf,
        Ymax=np.Inf,
        **kwargs,
    ):
        """
        Construct class from netcdf. The netcdf has coordinates x, y and a third axis that represents time of which
        the name is defined by the keyword argument time_axis.
        """
        source = Path(nc_file)
        ds = xr.open_dataarray(source)
        model = cls.clip_ds(cls, ds, Xmin, Xmax, Ymax, Ymin, ret=True)

        return cls(source, model, time_axis)

    def generate(self):
        """
        Generate instances of Surface
        """
        for timestep in self.timesteps:
            source = "Timestep of " + self.source.name
            yield (Surface(source, self.model.sel({self.time_axis: timestep})))

    def to_vtk(
        self,
        folder=None,
        base_filename="",
        project=None,
        bottom=None,
        binary=True,
        method="pyvista",
    ):
        """
        Loop through each Surface object using self.generate() and use
        the to_vtk method of each instance to write the timestep to vtk.
        """
        if folder is None:
            folder = Path(__file__).parents[2].joinpath(Path(f"results\\vtk\\"))

        for surface_timestep in self.generate():
            time_string = np.datetime_as_string(
                surface_timestep.model[self.time_axis].values, unit="D"
            )
            file = folder.joinpath(base_filename + "_" + time_string + ".vtk")

            if not xr.ufuncs.isnan(  # pylint: disable=no-member
                surface_timestep.model
            ).all():
                if method == "pyvista":
                    surface_timestep.to_vtk(
                        file=file,
                        project=project,
                        bottom=bottom,
                        binary=binary,
                        method="pyvista",
                    )
                elif method == "geovtk":
                    surface_timestep.to_vtk(
                        file=file,
                        project=project,
                        bottom=bottom,
                        method="geovtk",
                    )


class RGBSurface(Raster):
    """
    Base class for RGB Surfaces. An RGBSurface is an Xarray.DataSet with coordinates
    'x' and 'y' and contains data_vars for Red, Green and Blue values.
    This is e.g. a georeferenced image like a satellite image.
    """

    def __init__(self, source, model):
        super().__init__(self, source, model)
