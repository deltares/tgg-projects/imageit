import sys

sys.path.insert(0, r".\geo_vtk\src")
from vtkclass import VtkClass  # pylint: disable=import-error
from base import Raster  # pylint: disable=import-error
import numpy as np
import xarray as xr
import pyvista as pv
import glob
from pathlib import Path


class GeologicalVoxelModel(Raster):
    """
    Class for geological voxel model, represented as Xarray.DataArray

    The dataarray must have x, y and z coordinates

    self.model is the only mutable attribute in this class, See slots in GeologicalModel base class.
    The rest are properties that are derived from the model dataset.
    """

    def __init__(self, source, model, data_vars):
        super().__init__(source, model)
        self.data_vars = data_vars

    @property
    def zc(self):
        """
        Get the z coordinates as np.array
        """
        return self.model.z.values

    @property
    def dz(self):
        """
        delta z. Resolution along z-axis
        """
        return self.zc[1] - self.zc[0]

    @property
    def Zmin(self):
        """
        Property Xmin derived from model. Since properties can't be explicitly set,
        this always represents the correct Zmin value for the current model.
        """
        return self.zc.min()

    @property
    def Zmax(self):
        """
        Property Xmin derived from model. Since properties can't be explicitly set,
        this always represents the correct Zmax value for the current model.
        """
        return self.zc.max()

    @property
    def origin(self):
        """
        Determine origin (bottom left corner of dataset) for setting pyvista grid origin attribute
        """
        # TODO: Check if coordinates need adjustment for midpoints (pyvista works with bottom left, xarray with midpoints)
        return (self.Xmin - 0.5 * self.dx, self.Ymin - 0.5 * self.dy, self.Zmin)

    @property
    def spacing(self):
        """
        Determine spacing (cell sizes along each axis) for setting pyvista grid spacing attribute
        """
        return (self.dx, self.dy, self.dz)

    @classmethod
    def from_netcdf(
        cls,
        nc_file,
        data_vars=["strat", "lithok"],
        Xmin=-np.Inf,
        Xmax=np.Inf,
        Ymin=-np.Inf,
        Ymax=np.Inf,
    ):
        """
        Load Geotop model from its official NetCDF distribution. This method is recommended and
        much faster than reading every layer from the .img files.

        data_var is e.g. 'lithok' for lithology or 'strat' for stratigraphy.
        """
        source = Path(nc_file)
        model = xr.open_dataset(nc_file)
        model = cls.clip_ds(
            cls, model, Xmin, Xmax, Ymin, Ymax, data_vars=data_vars, ret=True
        )
        return cls(source, model, data_vars)

    @classmethod
    def from_layers(cls, basis, Xmin=-np.Inf, Xmax=np.Inf, Ymin=-np.Inf, Ymax=np.Inf):
        """
        Load GeoTop from layers (.img files). New method reads layer depths directly from files and sorts them for
        further processing
        """
        layers_top = glob.glob(basis + "\\*.img")
        filenames = [Path(ps).stem for ps in layers_top]

        files_onder_nap = [f for f in filenames if "onder" in f]
        files_boven_nap = [f for f in filenames if "boven" in f]
        files_onder_nap.sort(key=lambda x: -float(x.split("_")[2]))
        files_boven_nap.sort(key=lambda x: float(x.split("_")[2]))

        filenames = files_onder_nap + files_boven_nap
        files = [basis + "/" + f + ".img" for f in files_onder_nap + files_boven_nap]

        zs = np.array(
            [-float(z.split("_")[2]) / 100 for z in filenames if "onder" in z]
            + [float(z.split("_")[2]) / 100 for z in filenames if "boven" in z]
        )

        assert len(zs) == len(layers_top)

        model0 = xr.open_rasterio(files[0]).squeeze()
        # Note Ymax is first instead of Ymin because Y-values are descending in the lyr dataarrays
        model0 = cls.clip_da(model0, Xmin, Xmax, Ymax, Ymin)

        array = np.zeros((len(model0.x), len(model0.y), len(layers_top)))
        model = xr.DataArray(
            array, coords={"x": model0.x, "y": model0.y, "z": zs}, dims=["x", "y", "z"]
        )

        for i, file in enumerate(files):
            model_i = xr.open_rasterio(file).squeeze()
            # Note Ymax is first instead of Ymin because Y-values are descending in the lyr dataarrays
            model_i = cls.clip_ds(model_i, Xmin, Xmax, Ymax, Ymin, ret=True)
            model[:, :, i] = model_i.values.transpose()
            print(i)

        model = model.where(model >= 0.0, other=np.nan)

        return cls(basis, model, None)

    def to_vtk(self, file=None, method="pyvista", binary=True):
        """
        Method to export a geological voxel model to vtk format using either geovtk or PyVista.
        """
        if file is None:
            file = (
                Path(__file__)
                .parents[2]
                .joinpath(
                    Path(f"results\\vtk\\" + f"{self.__class__.__name__}" + ".vtk")
                )
            )
        if method == "pyvista":
            self.__to_vtk_pyvista(file, binary=binary)
        elif method == "geovtk":
            self.__to_vtk_geovtk(file)

    def __to_vtk_pyvista(self, file, binary=True):
        self.pv_model = pv.UniformGrid()
        self.pv_model.dimensions = (
            np.array(self.model_as_array(data_var="lithok").shape) + 1
        )
        self.pv_model.origin = self.origin
        self.pv_model.spacing = self.spacing

        for data_var in self.data_vars:
            self.pv_model.cell_data[data_var] = self.model_as_array(
                data_var=data_var
            ).flatten(order="F")
        # Remove NAN-values, turning the pyvista uniform grid into a pyvista unstructured grid
        self.pv_model = self.pv_model.threshold()

        filename = self.__class__.__name__ + ".vtk"
        self.save_vtk(file, binary=binary)
        # self.pv_model.plot(show_edges=True, cmap='viridis')

    def __to_vtk_geovtk(self, file):
        vtk = VtkClass()
        vtk.make_3d_grid_to_vtk(
            file,
            self.model_as_array(),
            self.xc,
            self.yc,
            self.zc,
        )


class GeologicalLayerModel(Raster):
    """
    Class for geological Layer model, represented as Xarray.DataSet.

    The dataset must have x, y and layer coordinates

    self.model is the only mutable attribute in this class. See slots in GeologicalModel base class.
    The rest are properties that are derived from the model dataset.
    """

    @property
    def layer_codes(self):
        return dict(zip(self.model.layer.values, np.arange(0, len(self.model.layer))))

    def get_single_layer(
        self, layer_name, data_var_top="top", data_var_bottom="bottom", data_vars=None
    ):
        """
        Extract data of a single layer. The layer must be a valid coordinate of the Xarray.DataSet
        'top' and 'bottom' must be data_vars in the DataSet that give the layer's top and bottom position

        If no data_vars is given, the array of data will be filled with a value that corresponds to the layer name
        as defined by the dict 'self.layer_codes'.

        data_vars is a list-like with strings

        """
        if not layer_name in list(self.model.layer.values):
            raise IndexError(f"Layer name '{layer_name}' is not an existing coordinate")
        if (
            not all([dv in list(self.model.data_vars) for dv in data_vars])
            and data_vars is not None
        ):
            raise IndexError(f"One or more given data_vars not present in dataset")

        layer = self.model.sel(layer=layer_name)

        top = layer[data_var_top].values
        bottom = layer[data_var_bottom].values

        if data_vars is None:
            data = np.full_like(
                layer[data_var_top], fill_value=self.layer_codes[layer_name]
            )
        else:
            data = self.get_layer_data(layer, data_vars)

        return (layer_name.decode("UTF-8"), top, bottom, data)

    @staticmethod
    def get_layer_data(layer, data_vars):
        """
        Take a model layer and an iterable of data_vars and return the data as generator object
        """
        for data_var in data_vars:
            yield (data_var, layer[data_var].values)

    def get_all_layers(
        self, *data_vars, data_var_top="top", data_var_bottom="bottom", max_depth=-50
    ):
        """
        Generator object that yields the data of data_var for each layer coordinate.
        If *data_var is not given, the array of data will be filled
        with a code that represents the layer (determined by self.layer_codes). This is for e.g.
        building a model of the geological units of REGIS, rather than their respective
        hydrological parameters.

        Also yields the layer name, data_var, top and bottom.

        Returns generator object.
        """
        if len(data_vars) > 0:
            data_vars = data_vars[0]
        else:
            data_vars = None

        for layer_name in self.model.layer.values:
            _, top, bottom, data = self.get_single_layer(
                layer_name,
                data_var_top=data_var_top,
                data_var_bottom=data_var_bottom,
                data_vars=data_vars,
            )
            if np.isfinite(top).any() and (top > max_depth).any():
                yield (layer_name.decode("UTF-8"), data_vars, top, bottom, data)

    def to_vtk(
        self, layer_name, top, bottom, data, file=None, return_result=False, binary=True
    ):
        self.pv_model = self.create_basic_structured_grid(array=top)

        pv_top = self.pv_model.points.copy()
        pv_bottom = self.pv_model.points.copy()
        pv_bottom[:, -1] = bottom.flatten(order="F")
        self.pv_model.points = np.vstack((pv_top, pv_bottom))
        self.pv_model.dimensions = [*self.pv_model.dimensions[0:2], 2]

        data_vars = []
        pv_models = []
        for data_var, array in data:
            data_vars.append(data_var)
            data_flat = array.flatten(order="F")
            self.pv_model.point_data[data_var] = np.hstack((data_flat, data_flat))

        if self.pv_model.n_arrays == 1:
            self.pv_model = self.pv_model.threshold()
        else:
            self.pv_model = self.pv_model.cast_to_unstructured_grid()

        if return_result:
            return self.pv_model

        data_vars_string = "_".join([dv for dv in data_vars])
        filename = self.__class__.__name__ + f"_{layer_name}_{data_vars_string}.vtk"
        self.save_vtk(self.pv_model, filename=filename, binary=binary)
        # self.pv_model.plot()

    def to_vtk_all(self, layer_generator, separate_files=False, binary=True):
        volumes = []
        for layer_name, data_vars, top, bottom, data in layer_generator:
            if separate_files:
                # Return a single VTK-file for each layer
                self.to_vtk(layer_name, top, bottom, data, binary=binary)
            else:
                volumes.append(
                    self.to_vtk(layer_name, top, bottom, data, return_result=True)
                )

        if not separate_files:
            data_vars_string = "_".join([dv for dv in data_vars])
            combined_volume = volumes[0].merge(volumes[1:])
            # combined_volume = combined_volume.cast_to_unstructured_grid()
            filename = self.__class__.__name__ + f"_all_{data_vars_string}.vtk"
            self.save_vtk(combined_volume, filename, binary=binary)
