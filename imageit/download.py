from pathlib import Path
import geopandas as gpd
import cv2
from owslib.wfs import WebFeatureService
from owslib.wms import WebMapService
from utils import (  # pylint: disable=import-error
    ahn_gml_to_gdf,
    select_tiles,
    download_and_unzip,
    combine_tiffs,
    find_max_resolution,
    decode_image_bytestring,
)  # pylint: disable=import-error

misc_destination = Path(__file__).parents[1].joinpath(r"data\misc")

# TODO: maybe to class structure, because the writing and then reading again is a bit lame


def WMS_download(WMS, feature, xmin, xmax, ymin, ymax, desired_res=1, max_cells=1.5e7):
    wms = WebMapService(WMS, version="1.1.1")
    response = wms[feature]
    wms[feature].crsOptions

    x_cells = (xmax - xmin) / desired_res
    y_cells = (ymax - ymin) / desired_res

    x_cells, y_cells, resolution = find_max_resolution(
        x_cells, y_cells, desired_res, max_cells
    )

    # Format GeoTiff unavail unfortunately
    img = wms.getmap(
        layers=[feature],
        srs="EPSG:28992",
        bbox=(xmin, ymin, xmax, ymax),
        size=(x_cells, y_cells),
        format="image/jpeg",
        transparent=True,
    )

    decoded_img = decode_image_bytestring(img)
    return decoded_img
    # out = open(misc_destination.joinpath("temp.jpg"), "wb")
    # out.write(img.read())
    # out.close()

    # im = cv2.imread(str(misc_destination.joinpath("temp.jpg")))
    # img = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
    # img.shape


def AHN_kaartblad(
    WFS="https://geodata.nationaalgeoregister.nl/ahn3/wfs", ahn="ahn3", **kwargs
):
    wfs = WebFeatureService(WFS, version="1.1.0")
    bladindex_id = [f for f in list(wfs.contents) if "bladindex" in f][0]
    response = wfs.getfeature(typename=bladindex_id)

    gml_file = misc_destination.joinpath(ahn + "_kaartbladen.gml")
    with open(gml_file, "wb") as gml:
        gml.write(response.read())

    gdf = ahn_gml_to_gdf(gml_file, ahn)
    gdf.to_file(misc_destination.joinpath(ahn + "_kaartbladen.gpkg"), driver="GPKG")


def AHN_tiles(
    bounds,
    destination_path=misc_destination,
    ahn="ahn3",
    res="5",
    model="dtm",
    return_result=False,
    **kwargs
):
    gdf = gpd.read_file(misc_destination.joinpath(ahn + "_kaartbladen.gpkg"))
    gdf = select_tiles(gdf, bounds)

    base_url = "https://download.pdok.nl/rws/ahn3/v1_0/"
    dest = destination_path.joinpath("ahn_tiles")

    if not dest.is_dir():
        Path.mkdir(dest)

    for i, row in gdf.iterrows():
        if res == "5":
            filename = "M5_" + row["bladnr"].upper() + ".ZIP"
        else:
            filename = "M_" + row["bladnr"].upper() + ".ZIP"
        model_line = res + "m_" + model
        url = base_url + model_line + "/" + filename
        print("Downloading " + url)
        download_and_unzip(url, dest)

    tiffs = dest.glob("*.TIF")
    if return_result:
        ds = combine_tiffs(tiffs, dest.joinpath("mosaic.nc"), ret=True)
        [Path.unlink(t) for t in tiffs]
        return ds
    else:
        combine_tiffs(tiffs, dest.joinpath("mosaic.nc"), ret=False)
