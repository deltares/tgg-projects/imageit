"""
Welcome to the ImageIT cookbook. Here you can find examples to help you use the package

Only the functions that were refactored in the Erik_Dev version are presented here

TODO: make Jupyter notebook
"""

from pathlib import Path

from models import (  # pylint: disable=import-error
    GeologicalVoxelModel,
    GeologicalLayerModel,
    Surface,
    SurfaceTimeSeries,
    RGBSurface,
    ValuePoints,
    Texture,
)  # pylint: disable=import-error

Xmin = 16000
Xmax = 17700
Ymin = 383200
Ymax = 384500

ws = Surface.from_raster_file(
    r"p:\11203725-modelberekeningen\04_multibeam\02_processed_data\ws_monding_mosaic.tiff",
    band=1,
    Xmin=Xmin,
    Xmax=Xmax,
    Ymin=Ymin,
    Ymax=Ymax,
)
ws.to_vtk(
    r"p:\11203725-modelberekeningen\04_multibeam\02_processed_data\ws_monding_mosaic.vtk"
)


"""
Let's first define the bounds of the area that we will work in:
"""
Xmin = 130000
Xmax = 138000
Ymin = 441500
Ymax = 446500

# luchtfoto = Texture.from_wms(xmin=Xmin, xmax=Xmax, ymin=Ymin, ymax=Ymax)

pnts = ValuePoints.from_xyzfile(
    r"c:\Users\onselen\Downloads\20210601_PES WAAL__S1_Site 2_25cm_RD_NAP.txt"
)
pnts.to_vtk(reconstruct_surface=True, binary=True)


"""
Example of exporting AHN to VTK. The from_downloaded_ahn_tiles automatically downloads the AHN tiles 
that overlap with the study area bounds and creates a mosaic of this surface
"""
ahn = Surface.from_downloaded_ahn_tiles(Xmin=Xmin, Xmax=Xmax, Ymin=Ymin, Ymax=Ymax)
ahn.to_vtk(binary=True)

# For all instances of objects in this package, the 'model' attribute is the internal data format. In this case an Xarray.DataArray.
# The 'pv_model', once created, is the PyVista model representation of the data (which can be directly exported to VTK)
# Check them out to see what they look like:
model = ahn.model
pv_model = ahn.pv_model


"""
You can create a GenericSurface object from a local raster file. In this example the bottom of the Kreftenheye Wijchen member (a geological unit)
"""
st_krwy = Surface.from_raster_file(
    Path(__file__)
    .parents[1]
    .joinpath(
        r"p:\11206817-datagedreven-schematisati\WP 1 - Data and conceptual model\Steering planes\channelbelt_bool.tif"
    ),
    band=1,
    Xmin=Xmin,
    Xmax=Xmax,
    Ymin=Ymin,
    Ymax=Ymax,
)
st_krwy.to_vtk(
    r"p:\11206817-datagedreven-schematisati\WP 1 - Data and conceptual model\Steering planes\channelbelt_bool.vtk"
)


"""
If you have a netCDF of surfaces along a time axis (i.e. a timeseries), you can export the timesteps as vtk
The timestep will be added to the base_filename. e.g. texelstroom_bathy_2016_01_01.vtk is one of the outputs in the below example.
The 'bottom' argument is given to turn the model into a solid block, rather than just the surface.
The height of 'bottom' must be below the minimum height that is ever encountered in one of the timesteps.
This argument can also be added to GenericSurface.to_vtk

Bounding box is not given as this data is already clipped
"""
bath = SurfaceTimeSeries.from_netcdf(
    Path(__file__).parents[1].joinpath(r"data\example_data\texelstroom.nc"),
    time_axis="year",
)
bath.to_vtk(base_filename="texelstroom_bathy", bottom=-26)


"""
You can export a point shapefile or geopackage to vtk using the ValuePoints class and the from_vectorfile constructor.
In this example we load InSar data and want to visualise the field 'VELOCITY'. Since InSar points represent reflectors
at the surface, we would like project the points on our ahn elevation.
"""
insar = ValuePoints.from_vectorfile(
    Path(__file__).parents[1].joinpath(r"data\example_data\insar.gpkg"),
    "VELOCITY",
    elevation=ahn,
    desired_crs=28992,
)
# Use z_source = "elevation" to use the elevation of ahn for the points. Leaving it blank or using "values"
# will set the point's height to their values (in this case the insar trend)
insar.to_vtk(z_source="elevation")


"""
You can load and export GeoTop from it's official netCDF distribution by using the Geotop.from_netcdf constructor.
You can choose the data_vars to export. In this case (and by default) it exports the 'strat' and 'lithok' data_vars.

When you don't give a filename argument to the 'to_vtk' method, the filename will be that of self.__class__.__name__, in this case 'Geotop'(.vtk)
"""
geotop = GeologicalVoxelModel.from_netcdf(
    r"p:\430-tgg-data\Geotop\Geotop14\geotop.nc",
    data_vars=["strat", "lithok"],
    Xmin=Xmin,
    Xmax=Xmax,
    Ymin=Ymin,
    Ymax=Ymax,
)
geotop.to_vtk()
